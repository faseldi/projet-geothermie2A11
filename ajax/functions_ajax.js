function showCorbeilles() {
	var url = 'ajax/getDevicesAjax.php';
    $.post(url, function(data){
        $('#corb').html(data);
        var posX = getElementsByClass("posX");
        var posY = getElementsByClass("posY");
        var posZ = getElementsByClass("posZ");
        var ids = getElementsByClass("idCorb");
        var noms = getElementsByClass("noms");
        for(var i = 0; i < posX.length; i++){
        	var x = document.getElementById(posX[i].id).textContent;
        	var y = document.getElementById(posY[i].id).textContent;
			var z = document.getElementById(posZ[i].id).textContent;
			var id = document.getElementById(ids[i].id).textContent;
			var nom = document.getElementById(noms[i].id).textContent;
	        addCorbeille(x,y,z,nom,id);
        }
        document.getElementById("corb").remove();
	});
}
function showCapteurs() {
    var url = 'ajax/getCapteursAjax.php';
    $.post(url, function(data){
        $('#capteurs').html(data);
        var posX = getElementsByClass("posX");
        var posY = getElementsByClass("posY");
        var posZ = getElementsByClass("posZ");
        var ids = getElementsByClass("ids");
        var idsD = getElementsByClass("idD");
        var noms = getElementsByClass("names");
        for(var i = 0; i < posX.length; i++){	        	
        	var x = document.getElementById(posX[i].id).textContent;
        	var y = document.getElementById(posY[i].id).textContent;
			var z = document.getElementById(posZ[i].id).textContent;
			var id = document.getElementById(ids[i].id).textContent;
			var idD = document.getElementById(idsD[i].id).textContent;
			var nom = document.getElementById(noms[i].id).textContent;
	        addCapteur(x,y,z,id,idD,nom);
        }
        document.getElementById("capteurs").remove();
    });
}
function addCorbeille(posX, posY, posZ, nom, id ){
	var loader = new THREE.OBJMTLLoader();
    loader.load( 'ajax/objects/corbeille.obj', 'ajax/objects/corbeille.mtl', function ( object ) {
		object.position.x = posX;
		object.position.y = posY-30; // 30 deci metres, la taille d'une corbeille
		object.position.z = posZ;
		object.name="corbeille";
		scene.add( object );
	});
	// création du cube transparent de la taille d'une corbeille pour intéragir avec celle-ci
	var corbeille = new THREE.BoxGeometry(3,30,3);
	var material = new THREE.MeshBasicMaterial( {color:0x00ff00, transparent: true, opacity:0} );
	var cube = new THREE.Mesh( corbeille, material );
	cube.name = nom;
	cube.identifiant = id;
	cube.position.x = posX;
	cube.position.y = posY-15;
	cube.position.z = posZ;
	scene.add( cube );
	objects.push ( cube );
	//console.log("une cobeille a été ajoutée en "+posX+" "+posY+" "+posZ);
}
function addCapteur(posX, posY, posZ, id, idD, nom ){
	var geometry = new THREE.SphereGeometry( 10, 10, 10 );
	var material = new THREE.MeshBasicMaterial( {color: 0x337ab7} );
	var sphere = new THREE.Mesh( geometry, material );
	sphere.name=nom;
	sphere.idD=idD;
	sphere.idC=id;
	sphere.position.x = posX;
	sphere.position.y = posY;
	sphere.position.z = posZ;
	sphere.scale.x = 0.1;
	sphere.scale.y = 0.1;
	sphere.scale.z = 0.1;
	scene.add( sphere );
	objects.push( sphere );
}
Element.prototype.remove = function() {
	this.parentElement.removeChild(this);
}
NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
	for(var i = this.length - 1; i >= 0; i--) {
		if(this[i] && this[i].parentElement) {
			this[i].parentElement.removeChild(this[i]);
		}
	}
}
function getElementsByClass(searchClass,node,tag) {
	var classElements = new Array();
	if ( node == null )
		node = document;
	if ( tag == null )
		tag = '*';
	var els = node.getElementsByTagName(tag);
	var elsLen = els.length;
	var pattern = new RegExp("(^|\\s)"+searchClass+"(\\s|$)");
	for (i = 0, j = 0; i < elsLen; i++) {
		if ( pattern.test(els[i].className) ) {
			classElements[j] = els[i];
			j++;
		}
	}
	return classElements;
}