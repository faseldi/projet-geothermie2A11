/*
*Copyright (C) 2015 by it's authors
*See AUTHORS
*
*This file is part of projet-geothermie.
*
*Projet-geothermie is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*Projet-geothermie is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with Projet-geothermie.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "PinFactory.h"

using namespace pins;

const int PinFactory::typeDigital = 0;
const int PinFactory::typeAnalog = 1;

Pin* PinFactory::createPin(int type, int pin){
    Pin* res;
    if(type == PinFactory::typeDigital){
        res = new DigitalPin(pin, 0);
    }else{
        res = new AnalogPin(pin);
    }
    return res;
}

