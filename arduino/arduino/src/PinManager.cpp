/*
*Copyright (C) 2015 by it's authors
*See AUTHORS
*
*This file is part of projet-geothermie.
*
*Projet-geothermie is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*Projet-geothermie is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with Projet-geothermie.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "PinManager.h"
#include <serstream>
#include <sstream>

using namespace std;
using namespace pins;

PinManager::PinManager(){
    factory = new PinFactory();
}

void PinManager::addPin(int pin,int type){
    pinList.push_back(factory->createPin(type,pin));
}


void PinManager::removePin(int type){

}


void PinManager::clear(){
    pinList.clear();
}


void PinManager::doMesure(){
    String mesures="";
    for(int i =0; i<pinList.size();i++){
        Pin* lePin = pinList.at(i);
        
        mesures += String(lePin->getPin()) + "," + String(lePin->read()) + ";";
        
    }
    send(mesures);
}

void PinManager::send(String message){

    Serial.print(message+"\r");
}

