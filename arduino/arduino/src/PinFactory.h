/*
*Copyright (C) 2015 by it's authors
*See AUTHORS
*
*This file is part of projet-geothermie.
*
*Projet-geothermie is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*Projet-geothermie is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with Projet-geothermie.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef H_PINS_PINFACTORY
#define H_PINS_PINFACTORY

#include "Arduino.h"
#include "analogpin.h"
#include "digitalpin.h"

namespace pins
{

    class PinFactory
    {

        public:

            /// methode qui crée un pin
            /// param type le type de pin
            /// param pin le numero du pin
            /// return le pointeur d'un pin
            Pin* createPin(int type,int pin);

            /// la constante du type analogique
            static const int typeAnalog;

            /// la constante du type numerique
            static const int typeDigital;

    };

}

#endif
