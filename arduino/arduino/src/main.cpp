/*
*Copyright (C) 2015 by it's authors
*See AUTHORS
*
*This file is part of projet-geothermie.
*
*Projet-geothermie is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*Projet-geothermie is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with Projet-geothermie.  If not, see <http://www.gnu.org/licenses/>.
*/

//Nécéssite la librairie StandardCplusPlus
//Ne compile temporairement qu'avec l'ide Arduino

#include "Arduino.h"
#include <StandardCplusplus.h>

#include "pin.h"
#include "PinFactory.h"
#include "PinManager.h"

unsigned long previousMillis = 0;
bool ledWaitState = false;
bool gotFirstCommand = false;

int BLUE_LED = 5;
int ORANGE_LED = 9;
int RED_LED = 10;

pins::PinManager pinManager;

/**
  * Make a LED blink
  * @var pin pin ID to make blink
  * @var interval time in milliseconds between every blink
  */
void blinkLed(int pin, int interval) {
    unsigned long currentMillis = millis();

    if (currentMillis - previousMillis > interval) {
        previousMillis = currentMillis;

        if (ledWaitState)
            digitalWrite(pin, LOW);
        else
            digitalWrite(pin, HIGH);
        
        ledWaitState = !ledWaitState;

    }
}

/**
  * Make a LED flash
  * @var pin pin ID to make flash
  * @var time how long in milliseconds the LED will stay on
  */
void flashLed(int pin, int time) {
    digitalWrite(pin, HIGH);
    delay(time);
    digitalWrite(pin, LOW);
}

// Initialisation.
void setup()
{
    Serial.begin(9600);
    pinMode(BLUE_LED, OUTPUT);
    pinMode(ORANGE_LED, OUTPUT);
    pinMode(RED_LED, OUTPUT);

    pinManager = pins::PinManager();
    
    Serial.print("HELLO\r"); //start communication
}

void loop()
{
    if (!Serial) {
        blinkLed(RED_LED, 100);
        return;
    }
    
    if (!gotFirstCommand)
        blinkLed(ORANGE_LED, 300);
    else
        digitalWrite(ORANGE_LED, HIGH);
}

/**
  * Send an error message to server
  * @var msg Message to send
  */
void sendError(String msg) {
    flashLed(RED_LED, 100);
    Serial.print("ERROR:"+msg+"\r");
}

/**
  * Parse PINS command received from server
  * @var str command received
  */
void parsePins(String str) {
    int commaIndex;
    int colonIndex;
    String pin;
    String mode;
    while (str.length() > 0) {
        commaIndex = str.indexOf(",");
        if (commaIndex == -1) {
            sendError("002,Bad syntax");
            return;
        }
        pin = str.substring(0, commaIndex);
        colonIndex = str.indexOf(";");
        if (colonIndex == -1) {
            sendError("002,Bad syntax");
            return;
        }
        mode = str.substring(commaIndex+1, colonIndex);
        str = str.substring(colonIndex+1);

        if (mode == "A")
            pinManager.addPin(pin.toInt(), pins::PinFactory::typeAnalog);
        else if (mode == "D")
            pinManager.addPin(pin.toInt(), pins::PinFactory::typeDigital);
        else {
            sendError("003,Bad io_mode");
            return;
        }

    }
    Serial.print("OK\r");
}

/**
  * Read a main command received from server
  * @var cmd Command received
  */
void readCommand(String cmd) {
        if (cmd == "PING") {
            Serial.print("PONG\r");
        }
        else if (cmd == "READ") {
            pinManager.doMesure();
        }
        else if (cmd.startsWith("PINS:", 0)) {
            gotFirstCommand = true;
            parsePins(cmd.substring(5));
        }
        else {
            sendError("001,Command unknown");
        }
}

void serialEvent()
{
    flashLed(BLUE_LED, 10);
   
    String command = "";
    char inChar = 0;

    while (inChar != '\r') {
        while (!Serial.available()) {} //wait
        
        inChar = (char)Serial.read();
        if (inChar != '\r')
            command.concat(inChar);
    }

    if (command != "")
        readCommand(command);
}
