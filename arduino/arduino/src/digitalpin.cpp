/*
*Copyright (C) 2015 by it's authors
*See AUTHORS
*
*This file is part of projet-geothermie.
*
*Projet-geothermie is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*Projet-geothermie is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with Projet-geothermie.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "digitalpin.h"

using namespace pins;

DigitalPin::DigitalPin(int pin, int iomode)
: Pin(pin), m_iomode(iomode)
{
}

/*
DigitalPin::DigitalPin(const DigitalPin &digitalpin)
{
    *this = &digitalpin;
}*/

DigitalPin& DigitalPin::operator=(const DigitalPin &digitalpin)
{
    Pin::operator=(digitalpin);
    m_iomode = digitalpin.m_iomode;
    return *this;
}

int DigitalPin::getIoMode() const
{
    return m_iomode;
}

void DigitalPin::setup()
{
    pinMode(getPin(), m_iomode);
}

void DigitalPin::write(int value)
{
    if(m_iomode == OUTPUT)
        digitalWrite(getPin(), value);
}

int DigitalPin::read()
{
    if(m_iomode == INPUT)
        return digitalRead(getPin());
    else
        return LOW;
}

