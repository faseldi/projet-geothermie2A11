/*
*Copyright (C) 2015 by it's authors
*See AUTHORS
*
*This file is part of projet-geothermie.
*
*Projet-geothermie is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*Projet-geothermie is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with Projet-geothermie.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef H_PINS_PINMANAGER
#define H_PINS_PINMANAGER

#include <vector>
#include <string>
#include "Arduino.h"
#include "PinFactory.h"
#include "pin.h"


namespace pins
{
/// classe  qui gere les Pin de l'arduino
    class PinManager
    {

        public:
            /// Constructeur
            PinManager();

            /// methode d'ajout de pin
            /// \param pin le numero du pin
            /// \param type le type de pin (\see PinFactory)
            void addPin(int pin,int type);

            void removePin(int type);

            /// methode qui supprime tout les pins
            void clear();

            /// methode qui récupere les mesures et les envoient en serial
            void doMesure();

        private:
            PinFactory* factory;

            std::vector<Pin*> pinList;

            void send(String message);



    };

}

#endif
