/*
*Copyright (C) 2015 by it's authors
*See AUTHORS
*
*This file is part of projet-geothermie.
*
*Projet-geothermie is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*Projet-geothermie is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with Projet-geothermie.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef H_PINS_DIGITALPIN
#define H_PINS_DIGITALPIN

#include "pin.h"
#include "Arduino.h"

namespace pins
{

/// Pins numériques de l'Arduino.
/// Les pins numériques sont configurés pour être utilisé exclusivement soit en
/// entrée, soit en sortie à la construction.
class DigitalPin : public Pin
{
    public:

    /// Constructeur.
    /// \param pin Le numero de port physique sur l'Arduino.
    /// \param iomode Le mode d'E/S (OUTPUT ou INPUT).
    DigitalPin(int pin, int iomode);

    /// Constructeur de copie.
    /*DigitalPin(const DigitalPin&);*/

    /// Opérateur d'assignation.
    DigitalPin& operator=(const DigitalPin&);

    /// \return Le mode d'E/S de la pin (OUTPUT ou INPUT)
    int getIoMode() const;

    void    setup();

    /// Methode d'écriture.
    /// Le mode d'E/S doit être OUTPUT.
    /// \param value La valeur à écrire (HIGH ou LOW).
    void    write(int value);

    /// Methode de lecture.
    /// Le mode d'E/S doit être INPUT.
    /// \return La valeur lue (HIGH ou LOW).
    int     read();


    private:

    int m_iomode;
};

}

#endif
