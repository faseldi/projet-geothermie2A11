/*
*Copyright (C) 2015 by it's authors
*See AUTHORS
*
*This file is part of projet-geothermie.
*
*Projet-geothermie is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*Projet-geothermie is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with Projet-geothermie.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef H_PINS_PIN
#define H_PINS_PIN

#include "Arduino.h"

namespace pins
{

/// Classe de base abstraite pour la representation des pins de l'Arduino.
/// Déclare les methodes d'initialisation, d'écriture, et de lecture.
class Pin
{
    public:

    /// Constructeur.
    /// \param pin Le numero de port physique sur l'Arduino.
    Pin(int pin);

    /// Constructeur de copie.
    Pin(const Pin&);

    /// Opérateur d'assignation.
    Pin& operator=(const Pin&);

    /// \return Le numero de pin.
    int getPin() const;

    /// Methode d'initialisation.
    /// Appellée dans la methode principale setup de l'Arduino.
    virtual void setup() = 0;

    /// Methode d'écriture.
    /// \param value La valeur à écrire.
    virtual void write(int value) = 0;

    /// Methode de lecture.
    /// \return La valeur lue.
    virtual int read() = 0;


    private:

    int m_pin;

};

}

#endif

