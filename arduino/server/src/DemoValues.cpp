/*
*Copyright (C) 2015 by it's authors
*See AUTHORS
*
*This file is part of projet-geothermie.
*
*Projet-geothermie is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*Projet-geothermie is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with Projet-geothermie.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

#include "include/DemoValues.hpp"
#include "include/DBConnector.hpp"

DemoValues::DemoValues() {
    srand(time(NULL));
}

int DemoValues::randomIntelValue(int value) {
    return value + (rand()%10+1)-5;
}

void DemoValues::runDemo() {
    if (DBConnector::getInstance().isConnected) {
        cout << "\033[1;33mRunning demo : \033[0m" << endl;
        startSerial();
        int values[4] = {0, 5, 10, 15};
        cout << "Waiting for arduino to say Hello..." << endl;
        char * resp = readArduino();
        if (strcmp(resp, "HELLO\r") == 0) {
            cout << " \033[1;32mdone.\033[0m" << endl;
        }
        else {
            cout << "\033[1;31m" << resp << " received but HELLO excepted. Stopping\033[0m" << endl;
            return;
        }

        pingArduino();
        sendArduino("PINS:1,A;2,D;");
        resp = readArduino();
        if (strcmp(resp, "OK\r") != 0) {
            cout << "\033[1;31m" << resp << "\033[0m" << endl;
            return;
        }
        while(true) {
            valueArduino();
            for (int i=0; i < 4; i++) {
                values[i] = randomIntelValue(values[i]);
                DBConnector::getInstance().sendValue(i+1, values[i]);
                cout << values[i] << " added on " << i << endl;
            }
            sleep(2);
        }
    }
    else {
        cerr << "Cannot run demo without a configured database" << endl;
    }
}

void DemoValues::startSerial() {
    USB = open( "/dev/ttyACM0", O_RDWR| O_NOCTTY ); 

    struct termios tty;
    struct termios tty_old;
    memset (&tty, 0, sizeof tty);

    /* Error Handling */
    if ( tcgetattr ( USB, &tty ) != 0 ) {
       std::cout << "Error " << errno << " from tcgetattr: " << strerror(errno) << std::endl;
    }

    /* Save old tty parameters */
    tty_old = tty;

    /* Set Baud Rate */
    cfsetospeed (&tty, (speed_t)B9600);
    cfsetispeed (&tty, (speed_t)B9600);

    /* Setting other Port Stuff */
    tty.c_cflag     &=  ~PARENB;            // Make 8n1
    tty.c_cflag     &=  ~CSTOPB;
    tty.c_cflag     &=  ~CSIZE;
    tty.c_cflag     |=  CS8;

    tty.c_cflag     &=  ~CRTSCTS;           // no flow control
    tty.c_cc[VMIN]   =  1;                  // read doesn't block
    tty.c_cc[VTIME]  =  5;                  // 0.5 seconds read timeout
    tty.c_cflag     |=  CREAD | CLOCAL;     // turn on READ & ignore ctrl lines

    /* Make raw */
    cfmakeraw(&tty);

    /* Flush Port, then applies attributes */
    tcflush( USB, TCIFLUSH );
    if ( tcsetattr ( USB, TCSANOW, &tty ) != 0) {
       std::cout << "Error " << errno << " from tcsetattr" << std::endl;
    }
}

void DemoValues::pingArduino() {
    sendArduino("PING\r");
    readArduino();
}

char * DemoValues::valueArduino() {
    sendArduino("READ\r");
    return readArduino();
}

char * DemoValues::readArduino() {
    int n = 0,
        spot = 0;
    char buf = '\0';
 
    /* Whole response*/
    char * response = (char *) malloc(sizeof(char) * 1024);
    memset(response, '\0', sizeof response);

    do {
       n = read( USB, &buf, 1 );
       sprintf( &response[spot], "%c", buf );
       spot += n;

    } while( buf != '\r' && n > 0);

    if (n < 0) {
       std::cout << "Error reading: " << strerror(errno) << std::endl;
    }
    else if (n == 0) {
        std::cout << "Read nothing!" << std::endl;
    }
    else {
        std::cout << "\033[0;34mResponse: \033[0m" << response << std::endl;
    } 

    return response;
}

void DemoValues::sendArduino(const char cmd[]) {
    std::cout << "\033[0;33mSending: \033[0m" << cmd << std::endl;
    int n_written = 0,
            spot = 0;

    do {
            n_written = write( USB, &cmd[spot], 1 );
                spot += n_written;
    } while (cmd[spot-1] != '\r' && n_written > 0);
    std::cout << "\033[0;32msent.\033[0m" << std::endl;
}
