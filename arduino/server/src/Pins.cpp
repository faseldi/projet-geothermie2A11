/*
*Copyright (C) 2015 by it's authors
*See AUTHORS
*
*This file is part of projet-geothermie.
*
*Projet-geothermie is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*Projet-geothermie is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with Projet-geothermie.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>
#include <cppconn/resultset.h>
#include <cppconn/prepared_statement.h>
#include <cppconn/exception.h>

#include "include/Pin.hpp"
#include "include/Pins.hpp"
#include "include/DBInstance.hpp"
#include "include/DBConnector.hpp"

using namespace std;
using namespace sql;

Pins::Pins(vector<Pin> pins):DBInstance("capteur"), m_vectorPin(pins){}
	
bool Pins::updatePins(vector<Pin> vectorPin){
	m_vectorPin = vectorPin;
	return true; 
} 

int Pins::createPin(Pin pin, bool persist){
    int id = 1;
    if (persist) {
        try {
            PreparedStatement *pstmt = this->getNewPreparedStatement("INSERT INTO "+tableName+"(idD, typeC) VALUES(1, ?)");
            pstmt->setString(1, pin.getTypeStr());
            pstmt->execute();

            delete pstmt;
        }
        catch (SQLException &e) {
            DBConnector::getInstance().exception(e);
            return -1;
        }
        catch (exception &e) {
            cerr << e.what() << endl;
        }

        id = getLastInsertId();
        pin.setId(id);
    }
    else {
        if (contains(pin.getId())) {
            throw new runtime_error("Id already used");
            return -1;
        }
    }
	m_vectorPin.push_back(pin);
	return id;
}

int Pins::deletePin(int id){
	if ( !contains(id) ){
		return 0;
	}
	else{
		for ( int i = 0; i< m_vectorPin.size(); i++){
			if ( m_vectorPin[i].getId() == id ){
				//~ delete &m_vectorPin[i];
				m_vectorPin.erase(m_vectorPin.begin()+i);
				return 1;
			}
		}
		return 0;
	}
}

bool Pins::contains(int id){
    for ( int i = 0; i<  m_vectorPin.size(); i++){
        if ( m_vectorPin[i].getId() == id )
            return true;
    }
    return false;
}

Pin * Pins::getPinByPort(int p){
    for(Pin pin : m_vectorPin){
        if(pin.getPort()==p){
            return new Pin(pin);
        }
    }
    return NULL;
}

vector<Pin> Pins::getPins() const{ 
	return m_vectorPin;
}

void Pins::recoverPersisted() {
    sql::ResultSet* res = this->getNewPreparedStatement("SELECT * FROM "+tableName+" natural join branchement")->executeQuery();
    int total = 0;
    string typeC;
    int typeInt;
    while (res->next()) {
        total++;
        typeC = res->getString("typeC");
        if (typeC == "A")
            typeInt = 0;
        else if (typeC == "N")
            typeInt = 1;
        else
            typeInt = -1;


        this->createPin(Pin(res->getInt("idC"), typeInt, 0,res->getInt("port"), res->getBoolean("enregistre")), false);
    }
    delete res;
    cout << total << " pins found in database." << endl;
}
