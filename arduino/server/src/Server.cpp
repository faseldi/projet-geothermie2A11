/*
*Copyright (C) 2015 by it's authors
*See AUTHORS
*
*This file is part of projet-geothermie.
*
*Projet-geothermie is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*Projet-geothermie is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with Projet-geothermie.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>

#include "include/DBConnector.hpp"
#include "include/DemoValues.hpp"
#include "include/ArduinoConnector.hpp"
#include "include/Pins.hpp"
#include "include/Server.hpp"
#include "include/Configurator.hpp"


using namespace std;

    Server::Server(){
        Configurator::getInstance().readConfig("./config.json");

        DBConnector::getInstance().connect( Configurator::getInstance().getSettings()->get("db.host", "localhost"),
                                            Configurator::getInstance().getSettings()->get("db.user", "root"),
                                            Configurator::getInstance().getSettings()->get("db.password", "toor"),
                                            Configurator::getInstance().getSettings()->get("db.database", "geothermie") );

    }

    Server::~Server(){
    }

    void Server::createConnectors(){
        Pins pins = Pins(vector<Pin>());
        pins.recoverPersisted();
        ArduinoConnector::instance()->connect(pins); 
    }



/*int main(){
    Server *serveur = new Server();
    return 0;
}*/
