/*
*Copyright (C) 2015 by it's authors
*See AUTHORS
*
*This file is part of projet-geothermie.
*
*Projet-geothermie is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*Projet-geothermie is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with Projet-geothermie.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "include/ArduinoConnector.hpp"
#include "include/Configurator.hpp"
#include <string>
#include <iostream>
#include <time.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <sstream>

using namespace std;

ArduinoConnector* ArduinoConnector::m_instance = NULL;


ArduinoConnector::ArduinoConnector(){
   m_isConnected = false;
}


// Singleton, methode a utiliser pour instancier ArduinoConnector
ArduinoConnector* ArduinoConnector::instance(){
    if(m_instance==NULL){
	m_instance = new ArduinoConnector();
    }
    return m_instance;
}

void waiting(int time){

}

void ArduinoConnector::connect(Pins & pins){
    startSerial();
    cout << "Waiting for arduino to say Hello..." << endl;
    char * resp = readArduino();
    if (strcmp(resp, "HELLO\r") == 0) {
	cout << " \033[1;32mdone.\033[0m" << endl;
    }
    else {
	cout << "\033[1;31m" << resp << " received but HELLO excepted. Stopping\033[0m" << endl;
	return;
    }
    
    pingArduino();
    m_isConnected = true;
    sendConfig(sendPins(pins));

    while (true) {
        char * values = valueArduino();
        char * token;
        token = strtok(values, ";");
        while(token != NULL) {
            saveValue(token, pins);
            token = strtok(NULL, ";");
        }
         
        sleep(300);
    }
}

char * ArduinoConnector::sendPins(Pins & pins){
    ostringstream s;
    s<<"PINS:";
    for(Pin p  : pins.getPins()){
        if (p.isRecording())
            s<<p.getPort()<<","<<p.getTypeStr()<<";";
    }
    string str = s.str();
    str += '\r';
    return (char *)str.c_str();
}
        

void ArduinoConnector::sendConfig(const char cmd[]){
    sendArduino(cmd);
    char * resp = readArduino();
    if(strcmp(resp, "OK\r") != 0){
	cout << "\033[1;31m" << resp << "\033[0m" << endl;
    }
} 

void ArduinoConnector::pingArduino() {
    sendArduino("PING\r");
    readArduino();
}

char * ArduinoConnector::valueArduino() {
    sendArduino("READ\r");
    return readArduino();
}

char * ArduinoConnector::readArduino() {
    int n = 0,
        spot = 0;
    char buf = '\0';
 
    /* Whole response*/
    char * response = (char *) malloc(sizeof(char) * 1024);
    memset(response, '\0', sizeof response);

    do {
       n = read( USB, &buf, 1 );
       sprintf( &response[spot], "%c", buf );
       spot += n;

    } while( buf != '\r' && n > 0);

    if (n < 0) {
       cout << "Error reading: " << strerror(errno) << endl;
    }
    else if (n == 0) {
        cout << "Read nothing!" << endl;
    }
    else {
        cout << "\033[0;34mResponse: \033[0m" << response << endl;
    } 

    return response;
}

void ArduinoConnector::sendArduino(const char cmd[]) {
    cout << "\033[0;33mSending: \033[0m" << cmd << endl;
    int n_written = 0,
            spot = 0;

    do {
            n_written = write( USB, &cmd[spot], 1 );
                spot += n_written;
    } while (cmd[spot-1] != '\r' && n_written > 0);
    cout << "\033[0;32msent.\033[0m" << endl;
}



void ArduinoConnector::startSerial() {
    USB = open( Configurator::getInstance().getSettings()->get("arduino.serial", "/dev/ttyACM0").c_str(), O_RDWR| O_NOCTTY ); 

    struct termios tty;
    struct termios tty_old;
    memset (&tty, 0, sizeof tty);

    /* Error Handling */
    if ( tcgetattr ( USB, &tty ) != 0 ) {
       cout << "Error " << errno << " from tcgetattr: " << strerror(errno) << endl;
    }

    /* Save old tty parameters */
    tty_old = tty;

    /* Set Baud Rate */
    cfsetospeed (&tty, (speed_t)B9600);
    cfsetispeed (&tty, (speed_t)B9600);

    /* Setting other Port Stuff */
    tty.c_cflag     &=  ~PARENB;            // Make 8n1
    tty.c_cflag     &=  ~CSTOPB;
    tty.c_cflag     &=  ~CSIZE;
    tty.c_cflag     |=  CS8;

    tty.c_cflag     &=  ~CRTSCTS;           // no flow control
    tty.c_cc[VMIN]   =  1;                  // read doesn't block
    tty.c_cc[VTIME]  =  5;                  // 0.5 seconds read timeout
    tty.c_cflag     |=  CREAD | CLOCAL;     // turn on READ & ignore ctrl lines

    /* Make raw */
    cfmakeraw(&tty);

    /* Flush Port, then applies attributes */
    tcflush( USB, TCIFLUSH );
    if ( tcsetattr ( USB, TCSANOW, &tty ) != 0) {
       cout << "Error " << errno << " from tcsetattr" << endl;
    }
}

void ArduinoConnector::saveValue(char * token, Pins & pins) {
    int pin;
    int value;
    float realValue;
    char * diff;
    int commaPos;
    char tmp[4];

    diff = strchr(token, ',');
    commaPos = (int)(diff - token);

    if (commaPos > 0) {
        strncpy(tmp, token, commaPos);
        pin = stoi(string(tmp));

        strncpy(tmp, token+commaPos+1, strlen(token));
        value = stoi(string(tmp));

        realValue = (value*500)/1024;

        pins.getPinByPort(pin)->sendValue(realValue);
    }
    
}

