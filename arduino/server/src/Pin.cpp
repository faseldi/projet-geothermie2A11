/*
*Copyright (C) 2015 by it's authors
*See AUTHORS
*
*This file is part of projet-geothermie.
*
*Projet-geothermie is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*Projet-geothermie is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with Projet-geothermie.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <iostream>
#include <string>
#include <cppconn/resultset.h>
#include <cppconn/prepared_statement.h>
#include <cppconn/exception.h>

#include "include/Pin.hpp"
#include "include/DBConnector.hpp"

using namespace std;

//
Pin::Pin(int id, int type,float iomode, int port, bool record):
    m_id(id),m_type(type),m_iomode(iomode), port(port), record(record) {}
//
int Pin::getType() const{
	return m_type;
}

int Pin::getPort()const{
    return port;
}
void Pin::setPort(int p){
    port=p;
}
bool Pin::isRecording(){
    return record;
}
string Pin::getTypeStr() const{
    if (m_type == 0)
        return "A";
    else if (m_type == 1)
        return "D";
    else
        return "";
}
//
float Pin::getIomode() const{
	return m_iomode;
}
//
int Pin::getId() const{
	return m_id;
}
//
string Pin::toString(){
	int res = this -> getId();
	return ""+res;
}

void Pin::sendValue(float value) {
    DBConnector::getInstance().sendValue(m_id, value);
}
