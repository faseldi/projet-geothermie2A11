/*
*Copyright (C) 2015 by it's authors
*See AUTHORS
*
*This file is part of projet-geothermie.
*
*Projet-geothermie is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*Projet-geothermie is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with Projet-geothermie.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "include/DBInstance.hpp"

#include <string>
#include <cppconn/prepared_statement.h>
#include <cppconn/connection.h>
#include <cppconn/resultset.h>

#include "include/DBConnector.hpp"

DBInstance::DBInstance(string tableName) {
    this->tableName = tableName;
}

string DBInstance::getTableName() const {
    return tableName;
}

PreparedStatement* DBInstance::getNewPreparedStatement(string req) const {
    if (DBConnector::getInstance().isConnected) {
        return DBConnector::getInstance().getConnection()->prepareStatement(req);
    }
    else {
        throw new std::runtime_error("Database is not connected");
        return NULL;
    }
}

int DBInstance::getLastInsertId() {
    sql::ResultSet* res = this->getNewPreparedStatement("SELECT LAST_INSERT_ID();")->executeQuery();
    res->next();
    return res->getInt(1);
}
    
