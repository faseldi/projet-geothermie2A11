/*
*Copyright (C) 2015 by it's authors
*See AUTHORS
*
*This file is part of projet-geothermie.
*
*Projet-geothermie is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*Projet-geothermie is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with Projet-geothermie.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <string>
#include <cstdio>

#include "include/DBConnector.hpp"
#include "include/DateTime.hpp"

using namespace std;

DBConnector::DBConnector() {
    try {
        driver = get_driver_instance();
    }
    catch(SQLException &e) {
        exception(e);
    }

    isConnected = false;
    
}

DBConnector& DBConnector::getInstance() {
    static DBConnector instance;

    return instance;
}

void DBConnector::connect(string host, string user, string password, string database) {
    try {
        con = driver->connect("tcp://"+host, user, password);
        con->setSchema(database);
        this->con = con;
        isConnected = true;
    }
    catch(SQLException &e) {
        exception(e);
    }

}

void DBConnector::disconnect() {
    try {
        con->close();   
        delete con;

        driver->threadEnd();
        
        isConnected = false;
    }
    catch(SQLException &e) {
        exception(e);
    }
}

void DBConnector::sendValue(int idC, float data, DateTime dt) {
    try {
        pstmt = con->prepareStatement("INSERT INTO donnees(idC, date, valeur) VALUES(?, ?, ?)");
        pstmt->setInt(1, idC);
        pstmt->setDateTime(2, dateTimeToString(dt));
        pstmt->setDouble(3, data);
        pstmt->execute();
        pstmt->close();
        delete pstmt;
    }
    catch(SQLException &e) {
        exception(e);
    }

    try {
        pstmt = con->prepareStatement("UPDATE branchement SET valeurActuelle=? WHERE idC=?");
        pstmt->setDouble(1, data);
        pstmt->setInt(2, idC);
        pstmt->execute();
        pstmt->close();
        delete pstmt;
    }
    catch(SQLException &e) {
        exception(e);
    }
}

void DBConnector::sendValue(int idC, float data) {
    sendValue(idC, data, DateTime::now());
}

void DBConnector::exception(SQLException &e) {
    cout << "ERROR: SQLException in " << __FILE__;
    cout << " (" << __func__ << ") on line " << __LINE__ << endl;
    cout << "ERROR: " << e.what();
    cout << " (MySQL error code: " << e.getErrorCode();
    cout << ", SQLState: " << e.getSQLState() << ")" << endl;
}

string DBConnector::dateTimeToString(DateTime date) {
    ostringstream stringStream;
    stringStream << date.year() << "-" << date.month() << "-" << date.day() << " " << date.hour() << ":" << date.minute() << ":" << date.second();
    return stringStream.str();
}

Connection* DBConnector::getConnection() {
    if (!isConnected)
        return NULL;
    else
        return con;
}
