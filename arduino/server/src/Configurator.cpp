/*
*Copyright (C) 2015 by it's authors
*See AUTHORS
*
*This file is part of projet-geothermie.
*
*Projet-geothermie is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*Projet-geothermie is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with Projet-geothermie.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <string>
#include <sstream>
#include <boost/property_tree/json_parser.hpp>
#include <boost/foreach.hpp>

#include "include/Configurator.hpp"

using namespace std;
using namespace boost::property_tree;


Configurator::Configurator() {
    pt = new ptree();
}

Configurator& Configurator::getInstance() {
    static Configurator instance;

    return instance;
}

bool Configurator::readConfig(string file) {
    try {
        json_parser::read_json(file, *pt);
    }
    catch (std::exception const& e) {
        cerr << e.what() << endl;
        return false;
    }
    return true;
}

ptree* Configurator::getSettings() {
    return pt;
}
