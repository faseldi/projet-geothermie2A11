/*
*Copyright (C) 2015 by it's authors
*See AUTHORS
*
*This file is part of projet-geothermie.
*
*Projet-geothermie is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*Projet-geothermie is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with Projet-geothermie.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef H_PIN
#define H_PIN
#include <string>

class Pin {
	
	private:
		int m_type;
		float m_iomode;
		int m_id;
        int port;
        bool record; 
	
	public:
		Pin(int id=0, int type=0,float iomode=0,int port=0, bool record=false);
		int getType() const;
        int getPort() const;
        void setPort(int p);
        bool isRecording();
        std::string getTypeStr() const;
		float getIomode() const;
		int getId() const;
        void setId(int id) { m_id = id; }
		std::string toString();
        
        void sendValue(float value);
};
#endif
