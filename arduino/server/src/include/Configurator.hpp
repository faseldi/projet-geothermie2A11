/*
*Copyright (C) 2015 by it's authors
*See AUTHORS
*
*This file is part of projet-geothermie.
*
*Projet-geothermie is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*Projet-geothermie is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with Projet-geothermie.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef DB_CONFIGURATOR_HPP
#define DB_CONFIGURATOR_HPP

#include <string>
#include <boost/property_tree/ptree.hpp>

using namespace std;

class Configurator {

    boost::property_tree::ptree* pt;

    public:
        static Configurator& getInstance();

        ///
        /// \brief Open the file to start reading config
        ///
        /// \param file Path to the file to read from
        ///
        /// \return bool Return true if able to read the file, false else
        bool readConfig(string file);

        boost::property_tree::ptree* getSettings();
    
    private:
        Configurator();
        Configurator(Configurator const&);
        void operator=(Configurator const&);
};

#endif
