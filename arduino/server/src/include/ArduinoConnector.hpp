/*
*Copyright (C) 2015 by it's authors
*See AUTHORS
*
*This file is part of projet-geothermie.
*
*Projet-geothermie is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*Projet-geothermie is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with Projet-geothermie.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef H_ARDUINOCONNECTOR
#define H_ARDUINOCONNECTOR

#include <string>
#include <vector>
#include "include/Pins.hpp"

class ArduinoConnector
{
    
public:

    int USB;
    
    // Methodes
    void connect(Pins & pins);
    static ArduinoConnector* instance(); // Instanciation unique
    void waiting(int time);
    char * readArduino();
    char * valueArduino();
    void sendConfig(const char cmd[]);
   
    char * sendPins(Pins & pins);
private:

    // singleton
    ArduinoConnector(); // Constructeur
    ArduinoConnector(const ArduinoConnector & );
    ArduinoConnector & operator=(const ArduinoConnector &);
    
    // attributs
    bool m_isConnected;
    static ArduinoConnector* m_instance;
    
    // Methodes
    //void send(std::vector<std::string> data) const;
    //std::vector<std::string> get(int pin) const;
    void pingArduino();
    void startSerial();
    void sendArduino(const char cmd[]);

    /**Parse un token de l'arduino puis
      *stocke la valeur en BD
      *@var token token de l'arduino
      */
    void saveValue(char * token, Pins & pins);
    
};

#endif
