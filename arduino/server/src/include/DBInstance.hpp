/*
*Copyright (C) 2015 by it's authors
*See AUTHORS
*
*This file is part of projet-geothermie.
*
*Projet-geothermie is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*Projet-geothermie is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with Projet-geothermie.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef DB_INSTANCE_HPP 
#define DB_INSTANCE_HPP

#include <string>
#include <cppconn/prepared_statement.h>

using namespace std;
using namespace sql;

class DBInstance {
    public:
        DBInstance(string tableName);

        ///
        /// \brief Return a PreparedStatement object with request
        /// \param req Request used by PreparedStatement
        /// \return PreparedStatement* New prepared statement with request
        ///
        PreparedStatement* getNewPreparedStatement(string req) const;

        string getTableName() const;
        ~DBInstance(){};
    
    protected:
        string tableName;
     
        ///
        /// \brief Recover data from database and create objects
        ///
        virtual void recoverPersisted(){};

        ///
        /// \brief Get the auto_increment id of the last inserted row
        ///
        /// \return int id
        int getLastInsertId();
        
};

#endif
