/*
*Copyright (C) 2015 by it's authors
*See AUTHORS
*
*This file is part of projet-geothermie.
*
*Projet-geothermie is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*Projet-geothermie is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with Projet-geothermie.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef H_PINS
#define H_PINS
#include <vector>

#include "Pin.hpp"
#include "DBInstance.hpp"

class Pins : public DBInstance {
	
	private:
		std::vector<Pin> m_vectorPin;
		bool updatePins(std::vector<Pin> vectorsPin);

	
	public:
		Pins(std::vector<Pin> vectorsPin=std::vector<Pin>());
        
            ///
            /// \brief Add a pin in pins collection
            /// \param pin The pin to add
            /// \param persist Persist the pin in DB
            ///
            /// \return int id The new ID of the pin if
            ///                persisted or 1 else
	    int createPin(Pin pin, bool persist=true);
		
            int deletePin(int id);
            Pin * getPinByPort(int p); 
            ///
            /// \brief Check if pins contains a pin with this id
            /// \param id Id to check
            ///
            /// \return bool True if contained, false else
	    bool contains(int id);
		
            std::vector<Pin> getPins() const;
        
            ///
            /// \brief Recover every object persisted on BD
            ///        as part of the collection.
            virtual void recoverPersisted() override;
};
#endif
