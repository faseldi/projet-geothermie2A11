/*
*Copyright (C) 2015 by it's authors
*See AUTHORS
*
*This file is part of projet-geothermie.
*
*Projet-geothermie is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*Projet-geothermie is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with Projet-geothermie.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef DB_CONNECTOR_HPP
#define DB_CONNECTOR_HPP

#include <string>

#include <cppconn/driver.h>
#include <cppconn/connection.h>
#include <cppconn/prepared_statement.h>
#include <cppconn/resultset.h>
#include <cppconn/exception.h>

#include "DateTime.hpp"

using namespace std;
using namespace sql;

class DBConnector {

    
    public:

        bool isConnected;

        static DBConnector& getInstance();
        void connect(string host, string user, string password, string database);
        void disconnect();
        void sendValue(int idC, float data, DateTime dt);
        void sendValue(int idC, float data);
        void send(string req);
        Connection* getConnection();
        static string dateTimeToString(DateTime date);
        static void exception(SQLException& );
    
    private:
    
        Driver *driver;
        Connection *con;
        Statement *stmt;
        PreparedStatement *pstmt;
        ResultSet *rs;

        DBConnector();
        DBConnector(DBConnector const&);
        void operator=(DBConnector const&);

};

#endif
