/*
*Copyright (C) 2015 by it's authors
*See AUTHORS
*
*This file is part of projet-geothermie.
*
*Projet-geothermie is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*Projet-geothermie is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with Projet-geothermie.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "gtest/gtest.h"
#include "include/DBConnector.hpp"
#include "include/Configurator.hpp"

class MysqlTest : public ::testing::Test {
    
    protected:
        virtual void SetUp() {
        }

        virtual void TearDown() {
        }
};
    
TEST_F(MysqlTest, Connection) {
    ASSERT_TRUE(Configurator::getInstance().readConfig("config_test.json"));
    DBConnector::getInstance().connect( Configurator::getInstance().getSettings()->get("db.host", "localhost"),
                                        Configurator::getInstance().getSettings()->get("db.user", "root"),
                                        Configurator::getInstance().getSettings()->get("db.password", "toor"),
                                        Configurator::getInstance().getSettings()->get("db.database", "geothermie") );
    EXPECT_TRUE(DBConnector::getInstance().isConnected);
}
