/*
*Copyright (C) 2015 by it's authors
*See AUTHORS
*
*This file is part of projet-geothermie.
*
*Projet-geothermie is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*Projet-geothermie is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with Projet-geothermie.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "gtest/gtest.h"
#include "include/DBConnector.hpp"
#include "include/Pins.hpp"

class TestPins : public ::testing::Test {
    protected:
        Pin* p1;
        Pin* p2;
        Pins* pins;

        virtual void SetUp() {
            p1 = new Pin(5,0,0);
            p2 = new Pin(7,1,1);

            pins = new Pins();
        }
        virtual void TearDown() {
            delete p1;
            delete p2;
            delete pins;
        }
};

TEST_F(TestPins, CreatePin) {

    EXPECT_EQ(5, p1->getId());
    EXPECT_EQ(7, p2->getId());
    EXPECT_EQ(0, p1->getType());
    EXPECT_EQ(1, p2->getType());
    EXPECT_EQ(0, p1->getIomode());
    EXPECT_EQ(1, p2->getIomode());
    EXPECT_EQ("A", p1->getTypeStr());
    EXPECT_EQ("D", p2->getTypeStr());

    p1->setId(6);

    EXPECT_EQ(6, p1->getId());
}

TEST_F(TestPins, PinsCollection) {

    EXPECT_FALSE(pins->contains(5));
   
    pins->createPin(*p1, false);
    ASSERT_TRUE(pins->contains(5));

    p2->setId(5);
    EXPECT_ANY_THROW(pins->createPin(*p2, false));

    pins->deletePin(5);
    EXPECT_FALSE(pins->contains(5));
}

TEST_F(TestPins, Persist) {
    int id = pins->createPin(*p1, true);
    ASSERT_NE(id, -1);

    Pins pins2 = Pins();
    pins2.recoverPersisted();
    ASSERT_TRUE(pins2.contains(id));
}
