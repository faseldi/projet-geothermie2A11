/*
*Copyright (C) 2015 by it's authors
*See AUTHORS
*
*This file is part of projet-geothermie.
*
*Projet-geothermie is free software: you can redistribute it and/or modify
*it under the terms of the GNU General Public License as published by
*the Free Software Foundation, either version 3 of the License, or
*(at your option) any later version.
*
*Projet-geothermie is distributed in the hope that it will be useful,
*but WITHOUT ANY WARRANTY; without even the implied warranty of
*MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*GNU General Public License for more details.
*
*You should have received a copy of the GNU General Public License
*along with Projet-geothermie.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "gtest/gtest.h"
#include "include/Configurator.hpp"
#include <boost/property_tree/ptree.hpp>

class TestConfigurator : public ::testing::Test {
    protected:

        virtual void SetUp() {
        }
        virtual void TearDown() {
        }
};

TEST_F(TestConfigurator, Read) {
    ASSERT_NO_THROW(Configurator::getInstance().readConfig("./config_test.json"));
    ASSERT_NE("__error__", Configurator::getInstance().getSettings()->get("db.host", "__error__"));
}
