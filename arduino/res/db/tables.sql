DROP TABLE IF EXISTS donnees;
DROP TABLE IF EXISTS branchement;
DROP TABLE IF EXISTS arduino;
DROP TABLE IF EXISTS capteur;
DROP TABLE IF EXISTS dispositif;

CREATE TABLE dispositif(
	idD		INT		(5) PRIMARY KEY AUTO_INCREMENT,
	nomD		VARCHAR		(40),
	typeD		VARCHAR		(30),
	lieu		VARCHAR		(40),
	posXD		INT		(5),
	posYD		INT		(5),
	posZD		INT		(5)
);

-- Mention pour les capteurs :
-- Il faut entrer dans la colonne type
-- le caractère 'A' si le capteur est de type analogique
-- ou 'N' si le capteur est de type numérique (= digital) 
CREATE TABLE capteur(
	idC		INT		(5) PRIMARY KEY AUTO_INCREMENT,
	idD		INT		(5) NOT NULL,
        nomC            VARCHAR         (40) NOT NULL,
	typeC		VARCHAR		(1) NOT NULL,
	unite		VARCHAR		(5),
	nivProfond	FLOAT		(5),
	posXC		INT		(5),
	posYC		INT		(5),	
	posZC		INT		(5),
        temps           INT             (10),
	CONSTRAINT FKidDCapteur FOREIGN KEY (idD)
		REFERENCES dispositif(idD)
);

CREATE TABLE arduino(
	idA		INT		(5) PRIMARY KEY AUTO_INCREMENT,
	nom		VARCHAR		(30),
        adMac           VARCHAR         (17) UNIQUE NOT NULL,
	nbPAnalog	INT		(2),
	nbPNum		INT		(2)
);

CREATE TABLE branchement(
	idA		INT		(5) NOT NULL,
	port		INT		(2) NOT NULL,
	idC		INT		(5) UNIQUE NOT NULL,
        valeurActuelle  FLOAT           (5),      
        enregistre      BOOLEAN,

	PRIMARY KEY (idA, idC),
        CONSTRAINT UNIQUE (idA, port),
	CONSTRAINT FKidABranchement FOREIGN KEY (idA)
		REFERENCES arduino(idA),
	CONSTRAINT FKidCBranchement FOREIGN KEY (idC)
		REFERENCES capteur(idC)
);

CREATE TABLE donnees(
	idC 		INT		(5) NOT NULL,
	date 		DATETIME	,
	valeur		FLOAT		(5),
	
	PRIMARY KEY (idC, date),
	CONSTRAINT FKidCDonnees FOREIGN KEY (idC)
		REFERENCES capteur(idC)
);

SOURCE triggers.sql
