
    "seed-queue-size": 9, 
    n SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Client :  localhost:8889
-- Généré le :  Mar 29 Septembre 2015 à 22:30
-- Version du serveur :  5.5.38
-- Version de PHP :  5.5.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de données :  `geothermie`
--

-- --------------------------------------------------------

--
-- Structure de la table `arduino`
--

DROP TABLE IF EXISTS `arduino`;
CREATE TABLE `arduino` (
  `idA` int(5) NOT NULL,
  `nom` varchar(30) DEFAULT NULL,
  `adMac` varchar(17) NOT NULL,
  `nbPAnalog` int(2) DEFAULT NULL,
  `nbPNum` int(2) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `arduino`
--

INSERT INTO `arduino` (`idA`, `nom`, `adMac`, `nbPAnalog`, `nbPNum`) VALUES
(1, 'Arduino1', '54g5fgs54', 3, 5),
(2, 'Arduino2', 'df54sf5gf4', 5, 2);

-- --------------------------------------------------------

--
-- Structure de la table `branchement`
--

DROP TABLE IF EXISTS `branchement`;
CREATE TABLE `branchement` (
  `idA` int(5) NOT NULL,
  `port` int(2) NOT NULL,
  `idC` int(5) NOT NULL,
  `valeurActuelle` float DEFAULT NULL,
  `enregistre` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `branchement`
--

INSERT INTO `branchement` (`idA`, `port`, `idC`, `valeurActuelle`, `enregistre`) VALUES
(1, 2, 29, NULL, NULL),
(1, 1, 32, NULL, 1),
(1, 0, 33, NULL, NULL),
(2, 3, 5, NULL, 0),
(2, 3, 6, NULL, 0),
(2, 0, 30, NULL, 0),
(2, 4, 34, NULL, NULL),
(2, 1, 50, NULL, 0);

-- --------------------------------------------------------

--
-- Structure de la table `capteur`
--

-- Mention pour les capteurs :
-- Il faut entrer dans la colonne type
-- le caractère 'A' si le capteur est de type analogique
-- ou 'N' si le capteur est de type numérique (= digital) 

DROP TABLE IF EXISTS `capteur`;
CREATE TABLE `capteur` (
  `idC` int(5) NOT NULL,
  `idD` int(5) NOT NULL,
  `nomC` varchar(40) NOT NULL,
  `typeC` varchar(1) NOT NULL,
  `unite` varchar(5) DEFAULT NULL,
  `nivProfond` float DEFAULT NULL,
  `posXC` int(5) DEFAULT NULL,
  `posYC` int(5) DEFAULT NULL,
  `posZC` int(5) DEFAULT NULL,
  `temps` int(10) DEFAULT '5'
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `capteur`
--

INSERT INTO `capteur` (`idC`, `idD`, `nomC`, `typeC`, `unite`, `nivProfond`, `posXC`, `posYC`, `posZC`, `temps`) VALUES
(1, 6, 'A1-1', 'A', 'C', 1, -720, 320, 140, 5),
(2, 6, 'A1-2.5', 'A', 'C', 2.5, -720, 170, 140, 5),
(3, 6, 'A1-4', 'A', 'C', 4, -720, 20, 140, 5),
(4, 6, 'A2-1', 'A', 'C', 1, -685, 320, 165, 5),
(5, 6, 'A2-2.5', 'A', 'C', 2.5, -685, 170, 165, 5),
(6, 6, 'A2-4', 'A', 'C', 4, -685, 20, 165, 5),
(7, 7, 'B1-1', 'A', 'C', 1, -550, 320, 250, 5),
(8, 7, 'B1-2.5', 'A', 'C', 2.5, -550, 170, 250, 5),
(9, 7, 'B1-4', 'A', 'C', 4, -550, 20, 250, 5),
(10, 8, 'C1-1', 'A', 'C', 1, -460, 320, 370, 5),
(11, 8, 'C1-2.5', 'A', 'C', 2.5, -460, 170, 370, 5),
(12, 8, 'C1-4', 'A', 'C', 4, -460, 20, 370, 5),
(13, 8, 'C2-1', 'A', 'C', 1, -460, 320, 430, 5),
(14, 8, 'C2-2.5', 'A', 'C', 2.5, -460, 170, 430, 5),
(15, 8, 'C2-4', 'A', 'C', 4, -460, 20, 430, 5),
(16, 9, 'D1-1', 'A', 'C', 1, -280, 320, 245, 5),
(17, 9, 'D1-2.5', 'A', 'C', 2.5, -280, 170, 245, 5),
(18, 9, 'D1-4', 'A', 'C', 4, -280, 20, 245, 5),
(19, 12, 'E1-1', 'A', 'C', 1, 300, 320, 355, 5),
(20, 12, 'E1-2.5', 'A', 'C', 2.5, 300, 170, 355, 5),
(21, 12, 'E1-4', 'A', 'C', 4, 300, 20, 355, 5),
(22, 2, 'REF-1', 'A', 'C', 1, 225, 320, 435, 5),
(23, 2, 'REF-1.5', 'A', 'C', 1.5, 225, 270, 435, 5),
(24, 2, 'REF-2', 'A', 'C', 2, 225, 220, 435, 5),
(25, 2, 'REF-2.5', 'A', 'C', 2.5, 225, 170, 435, 5),
(26, 2, 'REF-3', 'A', 'C', 3, 225, 120, 435, 5),
(27, 2, 'REF-3.5', 'A', 'C', 3.5, 225, 70, 435, 5),
(28, 2, 'REF-4', 'A', 'C', 4, 225, 20, 435, 5),
(29, 3, 'Air_exterieur', 'A', 'C', 0, -850, 550, 300, 5),
(30, 3, 'Air_sortie_PC', 'A', 'C', 0, -850, 480, 550, 5),
(31, 4, 'Tube 24m', 'A', 'C', 2, -494, 235, 550, 5),
(32, 4, 'Tube 18m', 'A', 'C', 2, -158, 260, 550, 5),
(33, 4, 'Tube 12m', 'A', 'C', 2, 178, 285, 550, 5),
(34, 4, 'Tube 6m', 'A', 'C', 2, 515, 310, 550, 5),
(35, 5, 'VMC Extraction', 'A', 'C', 0, -1000, 800, 450, 5),
(36, 5, 'VMC Insufflation', 'A', 'C', 0, -1000, 700, 450, 5),
(37, 5, 'VMC Rejet', 'A', 'C', 0, -1000, 800, 550, 5),
(38, 5, 'VMC Air neuf', 'A', 'C', 0, -1000, 700, 550, 5),
(39, 3, 'Air entree pc', 'A', 'C', 0, 850, 480, 550, 5),
(40, 15, '1', 'A', 'C', 1, 200, 200, 100, 5),
(41, 15, '2', 'A', 'C', 1, 200, 100, 100, 5),
(42, 15, '3', 'A', 'C', 1, 200, 0, 100, 5),
(43, 15, '4', 'A', 'C', 1, 200, -100, 100, 5),
(44, 15, '5', 'A', 'C', 1, 200, -200, 100, 5),
(45, 15, '6', 'A', 'C', 1, 200, -300, 100, 5),
(46, 15, '7', 'A', 'C', 1, 200, -400, 100, 5),
(47, 15, '8', 'A', 'C', 1, 200, -500, 100, 5),
(48, 15, '9', 'A', 'C', 1, 200, -600, 100, 5),
(49, 16, '1', 'A', 'C', 1, -80, 200, -200, 5),
(50, 16, '2', 'A', 'C', 1, -80, 100, -200, 5),
(51, 16, '3', 'A', 'C', 1, -80, 0, -200, 5),
(52, 16, '4', 'A', 'C', 1, -80, -100, -200, 5),
(53, 16, '5', 'A', 'C', 1, -80, -200, -200, 5),
(54, 16, '6', 'A', 'C', 1, -80, -300, -200, 5),
(55, 16, '7', 'A', 'C', 1, -80, -400, -200, 5),
(56, 16, '8', 'A', 'C', 1, -80, -500, -200, 5);

-- --------------------------------------------------------

--
-- Structure de la table `dispositif`
--

DROP TABLE IF EXISTS `dispositif`;
CREATE TABLE `dispositif` (
  `idD` int(5) NOT NULL,
  `nomD` varchar(40) DEFAULT NULL,
  `typeD` varchar(30) DEFAULT NULL,
  `lieu` varchar(40) DEFAULT NULL,
  `posXD` int(5) DEFAULT NULL,
  `posYD` int(5) DEFAULT NULL,
  `posZD` int(5) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `dispositif`
--

INSERT INTO `dispositif` (`idD`, `nomD`, `typeD`, `lieu`, `posXD`, `posYD`, `posZD`) VALUES
(1, 'puit', 'puit', 'puit', 850, 380, 550),
(2, 'REF', 'corbeille', 'puit', 225, 210, 435),
(3, 'Air', 'Air', 'puit', 850, 550, 300),
(4, 'Tube', 'Tube', 'puit', -494, 50, 550),
(5, 'VMC', 'VMC', 'puit', -494, 50, 550),
(6, 'A', 'corbeille', 'puit', -700, 210, 150),
(7, 'B', 'corbeille', 'puit', -550, 210, 250),
(8, 'C', 'corbeille', 'puit', -460, 210, 370),
(9, 'D1', 'corbeille', 'puit', -280, 210, 245),
(10, 'D2', 'corbeille', 'puit', 120, 210, 245),
(11, 'D3', 'corbeille', 'puit', 520, 210, 245),
(12, 'E1', 'corbeille', 'puit', -85, 210, 355),
(13, 'E2', 'corbeille', 'puit', 300, 210, 355),
(14, 'E3', 'corbeille', 'puit', 680, 210, 355),
(15, 'sonde80', 'sonde', 'sondes', 200, -400, 100),
(16, 'sonde70', 'sonde', 'sondes', -80, -350, -200),
(17, 'sonde70bis', 'sonde', 'sondes', -200, -350, 100);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `arduino`
--
ALTER TABLE `arduino`
  ADD PRIMARY KEY (`idA`), ADD UNIQUE KEY `adMac` (`adMac`);

--
-- Index pour la table `branchement`
--
ALTER TABLE `branchement`
  ADD PRIMARY KEY (`idA`,`idC`), ADD UNIQUE KEY `idC` (`idC`), ADD KEY `port_2` (`port`);

--
-- Index pour la table `capteur`
--
ALTER TABLE `capteur`
  ADD PRIMARY KEY (`idC`), ADD KEY `FKidDCapteur` (`idD`);

--
-- Index pour la table `dispositif`
--
ALTER TABLE `dispositif`
  ADD PRIMARY KEY (`idD`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `arduino`
--
ALTER TABLE `arduino`
  MODIFY `idA` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `capteur`
--
ALTER TABLE `capteur`
  MODIFY `idC` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=57;
--
-- AUTO_INCREMENT pour la table `dispositif`
--
ALTER TABLE `dispositif`
  MODIFY `idD` int(5) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `branchement`
--
ALTER TABLE `branchement`
ADD CONSTRAINT `FKidABranchement` FOREIGN KEY (`idA`) REFERENCES `arduino` (`idA`),
ADD CONSTRAINT `FKidCBranchement` FOREIGN KEY (`idC`) REFERENCES `capteur` (`idC`);

--
-- Contraintes pour la table `capteur`
--
ALTER TABLE `capteur`
ADD CONSTRAINT `FKidDCapteur` FOREIGN KEY (`idD`) REFERENCES `dispositif` (`idD`);

--
-- Structure de la table `donnees`
--

CREATE TABLE `donnees` (
  `idC` int(5) NOT NULL,
  `date` datetime NOT NULL DEFAULT '1970-01-01 00:00:00',
  `valeur` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `donnees`
--
ALTER TABLE `donnees`
  ADD PRIMARY KEY (`idC`,`date`);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `donnees`
--
ALTER TABLE `donnees`
ADD CONSTRAINT `FKidCDonnees` FOREIGN KEY (`idC`) REFERENCES `capteur` (`idC`);"seed-queue-size": 9, 
    "speed-limit-down": 100, 
    "speed-limit-down": 100; 




update dispositif set posXD="-50", posYD="-10", posZD="42" where nomD="D1";
update dispositif set posXD="-25", posYD="-10", posZD="66" where nomD="D2";
update dispositif set posXD="7", posYD="-10", posZD="42" where nomD="D3";
update dispositif set posXD="36", posYD="-10", posZD="66" where nomD="E1";
update dispositif set posXD="67", posYD="-10", posZD="42" where nomD="E2";
update dispositif set posXD="94", posYD="-10", posZD="66" where nomD="E3";
update dispositif set posXD="23", posYD="-10", posZD="76" where nomD="REF";
update dispositif set posXD="-117", posYD="-10", posZD="36" where nomD="A";
update dispositif set posXD="-110", posYD="-10", posZD="43" where nomD="B";
update dispositif set posXD="-82", posYD="-10", posZD="66" where nomD="C";
update capteur set posXC="-119", posYC="-10", posZC="34" where nomC="A1-1";
update capteur set posXC="-119", posYC="-25", posZC="34" where nomC="A1-2.5";
update capteur set posXC="-119", posYC="-40", posZC="34" where nomC="A1-4";
update capteur set posXC="-114", posYC="-10", posZC="38" where nomC="A2-1";
update capteur set posXC="-114", posYC="-25", posZC="38" where nomC="A2-2.5";
update capteur set posXC="-114", posYC="-40", posZC="38" where nomC="A2-4";
update capteur set posXC="-108", posYC="-10", posZC="45" where nomC="B1-1";
update capteur set posXC="-108", posYC="-25", posZC="45" where nomC="B1-2.5";
update capteur set posXC="-108", posYC="-40", posZC="45" where nomC="B1-4";
update capteur set posXC="-83", posYC="-10", posZC="68" where nomC="C1-1";
update capteur set posXC="-83", posYC="-25", posZC="68" where nomC="C1-2.5";
update capteur set posXC="-83", posYC="-40", posZC="68" where nomC="C1-4";
update capteur set posXC="-80", posYC="-10", posZC="63" where nomC="C2-1";
update capteur set posXC="-80", posYC="-25", posZC="63" where nomC="C2-2.5";
update capteur set posXC="-80", posYC="-40", posZC="63" where nomC="C2-4";
update capteur set posXC="-52", posYC="-10", posZC="44" where nomC="D1-1";
update capteur set posXC="-52", posYC="-25", posZC="44" where nomC="D1-2.5";
update capteur set posXC="-52", posYC="-40", posZC="44" where nomC="D1-4";
update capteur set posXC="38", posYC="-10", posZC="68" where nomC="E1-1";
update capteur set posXC="38", posYC="-25", posZC="68" where nomC="E1-2.5";
update capteur set posXC="38", posYC="-40", posZC="68" where nomC="E1-4";
update capteur set posXC="25", posYC="-10", posZC="78" where nomC="REF-1";
update capteur set posXC="25", posYC="-15", posZC="78" where nomC="REF-1.5";
update capteur set posXC="25", posYC="-20", posZC="78" where nomC="REF-2";
update capteur set posXC="25", posYC="-25", posZC="78" where nomC="REF-2.5";
update capteur set posXC="25", posYC="-30", posZC="78" where nomC="REF-3";
update capteur set posXC="25", posYC="-35", posZC="78" where nomC="REF-3.5";
update capteur set posXC="25", posYC="-40", posZC="78" where nomC="REF-4";
update capteur set posXC="85", posYC="-11.5", posZC="95" where nomC="Tube 6m";
update capteur set posXC="25", posYC="-14.5", posZC="95" where nomC="Tube 12m";
update capteur set posXC="-45", posYC="-18.5", posZC="95" where nomC="Tube 18m";
update capteur set posXC="-105", posYC="-21.5", posZC="95" where nomC="Tube 24m";
update capteur set posXC="-235", posYC="0", posZC="80" where nomC="1" and idD="15";
update capteur set posXC="-235", posYC="-105", posZC="80" where nomC="2" and idD="15";
update capteur set posXC="-235", posYC="-210", posZC="80" where nomC="3" and idD="15";
update capteur set posXC="-235", posYC="-315", posZC="80" where nomC="4" and idD="15";
update capteur set posXC="-235", posYC="-420", posZC="80" where nomC="5" and idD="15";
update capteur set posXC="-235", posYC="-525", posZC="80" where nomC="6" and idD="15";
update capteur set posXC="-235", posYC="-630", posZC="80" where nomC="7" and idD="15";
update capteur set posXC="-235", posYC="-735", posZC="80" where nomC="8" and idD="15";
update capteur set posXC="-235", posYC="-840", posZC="80" where nomC="9" and idD="15";
update capteur set posXC="-285", posYC="0", posZC="49" where nomC="1" and idD="16";
update capteur set posXC="-285", posYC="-103", posZC="50" where nomC="2" and idD="16";
update capteur set posXC="-285", posYC="-206", posZC="50" where nomC="3" and idD="16";
update capteur set posXC="-285", posYC="-309", posZC="50" where nomC="4" and idD="16";
update capteur set posXC="-285", posYC="-412", posZC="50" where nomC="5" and idD="16";
update capteur set posXC="-285", posYC="-515", posZC="50" where nomC="6" and idD="16";
update capteur set posXC="-285", posYC="-618", posZC="50" where nomC="7" and idD="16";
update capteur set posXC="-285", posYC="-721", posZC="50" where nomC="8" and idD="16";
update capteur set posXC="-150", posYC="28", posZC="100" where nomC="VMC Insufflation";
update capteur set posXC="-150", posYC="32", posZC="100" where nomC="VMC Extraction";
update capteur set posXC="-155", posYC="28", posZC="100" where nomC="VMC Air neuf";
update capteur set posXC="-155", posYC="32", posZC="100" where nomC="VMC Rejet";
update capteur set posXC="140", posYC="6", posZC="100" where nomC="Air entree pc";
update capteur set posXC="-142", posYC="6", posZC="101" where nomC="Air_sortie_PC";
update capteur set posXC="-142", posYC="6", posZC="40" where nomC="Air_exterieur";
