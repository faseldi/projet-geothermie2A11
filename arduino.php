<?php 
require_once "admin/GestionBase.php";
require_once "admin/loadSelect.php";
?>

<!--	
	<link rel="stylesheet" href="assets/css/arduinoBoard.css">
-->
	<link rel="stylesheet" href="assets/css/arduino.css">
	<script src="assets/vendor/jquery/jquery-1.9.1.min.js"></script>
	<link rel="stylesheet" href="assets/vendor/jquery/jquery-ui.css">
	<script src="assets/vendor/jquery/jquery-ui.js"></script>
	<link rel="stylesheet" href="assets/vendor/tabs/css/easy-responsive-tabs.css">
<link rel="stylesheet" href="assets/css/fonts/fonts.css">
<link rel="stylesheet" href="assets/vendor/c3/c3.min.css">
<link rel="stylesheet" href="assets/css/main.css">
	<script>
		$(document).ready(function()
		{
			$("#deviceName").change(function()
			{
				var formData = {device: this.value};
				$.ajax(
				{
					url : "admin/loadSensor.php",
					type: "POST",
					data : formData,
					success: function(data, status)
					{
						$("#resDevice").empty();
						$("#resDevice").html(data);
					}
				});
			});

			$("#arduinoName").change(function()
			{
				var value = {arduino: this.value};
				$.ajax(
				{
					url : "admin/loadArduino.php",
					type: "POST",
					data: value,
					success: function(data, status)
					{
						$("#resArduino").empty();
						$("#resArduino").html(data);
					}
				});
			});
			/*
			$.ajax(
			{
				url : "admin/timeUpdate.php",
				type : "POST",
				success: function(data, status)
				{
					$("#resConnected").html(data);
				}
			});*/
		});
	</script>
</head>
<body>
	<div id="global" class="container-fluid">
	    <div id="up">
	    <ul>
			<li id="selectArduino">
			<p>Choix de la carte</p>
			<?php getArduino(); ?>	
			</li>

			<li id="selectDevice">
			     <p>Choix du dispositif</p>
			     <?php getDevice(); ?>
			</li>

			<li id="time"><!--
			     <p>Gestion de l'intervalle de temps</p>
			     <div id="resConnected">
			     </div>-->
			     <div id="res2">
			     </div>
			</li>
		</ul>
	    </div>

	    <div id="resArduino">
		</div>


	</div>
	<div id="resDevice">
	</div>
	<a href="assets/images/arduino.png">Accéder à l'image des branchements Arduino</a>
</body>
</html>