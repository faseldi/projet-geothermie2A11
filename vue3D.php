<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<style type="text/css">
	body {
        font-family: Monospace;
        background-color: #f0f0f0;
        margin: 0px;
        overflow: hidden;
    }
  	.sonde-label {
        position: absolute;
        background-color: black;
        color:white;
        padding: 4px;
        font-family: "Gotham-Book", "HelveticaNeue", "Helvetica Neue", Helvetica, Arial, sans-serif;
        font-size: 14px;
        color: #fff;
    	background-color: #5bc0de;
    	border-color: #46b8da;
	    display: inline-block;
	    padding: 6px 12px;
	    margin-bottom: 0;
	    font-size: 14px;
	    font-weight: 400;
	    line-height: 1.42857143;
	    text-align: center;
	    white-space: nowrap;
	    vertical-align: middle;
	    background-image: none;
    	border: 1px solid transparent;
    	border-radius: 4px;
    	touch-action: manipulation;
    	cursor: pointer;
  	}
</style>
<body>
		<script type="text/javascript" src="ajax/Threejs/three.min.js"></script>
		<script type="text/javascript" src="ajax/Threejs/OBJLoader.js"></script>
		<script type="text/javascript" src="ajax/Threejs/jquery-1.9.1.js"></script>
		<script type="text/javascript" src="ajax/Threejs/Detector.js"></script>
		<script type="text/javascript" src="ajax/Threejs/stats.min.js"></script>
		<script type="text/javascript" src="ajax/Threejs/JSONLoader.js"></script>
		<script type="text/javascript" src="ajax/Threejs/OBJMTLLoader.js"></script>
		<script type="text/javascript" src="ajax/Threejs/MTLLoader.js"></script>
		<script type="text/javascript" src="ajax/Threejs/DDSLoader.js"></script>
		<script type="text/javascript" src="ajax/Threejs/CanvasRenderer.js"></script>
		<script type="text/javascript" src="ajax/Threejs/Projector.js"></script>
		<script type="text/javascript" src="ajax/functions_ajax.js"></script>
		<script type="text/javascript" src="ajax/Threejs/OrbitControls.js"></script>

<!-- Les divs vides pour l'ajax et la vue-->		
		<div id="vue">
			<div id="corb"></div>
			<div id="capteurs"></div>
		</div>
<!-- 			 -->
  <div id="3Dlabel" class="btn btn-info sonde-label"></div>
		<script>
			var container;
			var camera, scene, renderer, controls;
			var mouseX = 0, mouseY = 0;
			var windowHalfX = window.innerWidth / 2;
			var windowHalfY = window.innerHeight / 2;
			var raycaster = new THREE.Raycaster();
			var mouse = new THREE.Vector2();
			var objects = [];
			var selected = new Array();
			var label, labelIntersected;
			init();
			animate();


			function init() {
				container = document.getElementById("vue");
			    label = document.getElementById("3Dlabel");
				//document.body.appendChild( container );
				camera = new THREE.PerspectiveCamera( 45, window.innerWidth / window.innerHeight, 1, 1000 );
				camera.position.x = 0;
				camera.position.y = 45;
				camera.position.z = 400;

				controls = new THREE.OrbitControls( camera );
				controls.damping = 1;
				controls.zoomSpeed = 0.5;
				controls.addEventListener( 'change', render );
				scene = new THREE.Scene();
				var ambient = new THREE.AmbientLight( 0xffffff );
				scene.add( ambient );
				var manager = new THREE.LoadingManager();
				manager.onProgress = function ( item, loaded, total ) {
					console.log( item, loaded, total );
				};
				var loader = new THREE.OBJMTLLoader();
				loader.load( 'ajax/objects/Murs.obj', 'ajax/objects/Murs.mtl', function ( object ) {
					/* !!!! ATTENTION, WARNING !!!! */
					/* Changer la postion du mur risque d'influencer la position des objets dans la base de données */
					object.rotateY(3.14);
					object.position.y = 0;
					object.position.x = 145;
					object.position.z = 0;
					scene.add( object );
				});
				loader.load( 'ajax/objects/Vue2.obj', 'ajax/objects/Vue2.mtl', function ( object ) {
					/* !!!! ATTENTION, WARNING !!!! */
					/* Changer la postion du mur risque d'influencer la position des objets dans la base de données */
					object.position.y = 0;
					object.position.x = -350;
					object.position.z = 105;
					scene.add( object );
				});
				loader.load( 'ajax/objects/repere.obj', 'ajax/objects/repere.mtl', function ( object ) {
					object.position.y = 0;
					object.position.x = 0;
					object.position.z = 1;
					object.scale.x = 3;
					object.scale.y = 3;
					object.scale.z = 3;
					object.name="repere";
					scene.add( object );
				});
				if(Detector.webgl){
					renderer = new THREE.WebGLRenderer({ antialias:true });
				}else{
		            renderer = new THREE.CanvasRenderer(); 
		        }
		        renderer.setClearColor(0xffffff);
		        renderer.setPixelRatio(window.devicePixelRatio);
		        renderer.setSize(window.innerWidth, window.innerHeight);
        		container.appendChild(renderer.domElement);
				//container.addEventListener('mousemove', onDocumentMouseMove, false);
     		 
				// ajout des corbeilles
     			showCorbeilles();
     			showCapteurs();
				window.addEventListener('resize', onWindowResize, false);
				document.addEventListener('mousedown', onDocumentMouseDown , false);
        		document.addEventListener('mousemove', onDocumentMouseMove, false);
			}
	  function deplacerCurseur(vue){
	  	if(vue == 1){
			scene.getObjectByName("repere").position.x = 0;
			scene.position.x = 0;
	  	}
	  	if(vue == 2){
			scene.getObjectByName("repere").position.x = -270;
			scene.position.x = 250;
	  	}
	  }
      function onWindowResize() {
        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();
        renderer.setSize(window.innerWidth, window.innerHeight);
      }


      function onDocumentMouseMove(event) {
        mouse.x = (event.clientX / window.innerWidth) * 2 - 1;
        mouse.y = - (event.clientY / window.innerHeight) * 2 + 1;
        label.style.left = (event.clientX + 2) + 'px';
        label.style.top = (event.clientY - label.offsetHeight - 2) + 'px';
      }

	
      function onDocumentMouseDown(event) {
/*      	console.log(raycaster)
      	if(event.button == 1){
      	console.log(raycaster['ray']['origin'].x)
      	var x = raycaster['ray']['origin']['x'];
      	var y = raycaster['ray']['origin']['y'];
      	var z = raycaster['ray']['origin']['z'];
		camera.position.x = x;
		camera.position.y = y;
		camera.position.z = z;
		event.preventDefault();
		}
*/
		mouse.x = ( event.clientX / renderer.domElement.clientWidth ) * 2 - 1;
		mouse.y = - ( event.clientY / renderer.domElement.clientHeight ) * 2 + 1;

		raycaster.setFromCamera( mouse, camera );
		var intersects = raycaster.intersectObjects( objects );
		if ( intersects.length > 0 ) {
			console.log("c'est bon !!!, vous avez tapé : "+intersects[ 0 ].object.name);
			try{
				if(intersects[0].object.scale.x == 0.1){
					parent.cliqueCapteur(intersects [ 0 ].object.idC);
				}
			}catch(err){
				console.log('fonction cliqueCapteur(id) non définie sur cette page');
			}
			try{
				if(intersects[0].object.scale.x == 1){ // teste obligatoire pour différentier un clique sur capteur d'un clique sur corbeille
					parent.cliqueCorbeille(intersects [ 0 ].object.identifiant);
					return;
				}
			}catch(err){
				console.log('fonction cliqueCorbeille(id) non définie sur cette page');
				return;
			}
			intersects[ 0 ].object.material.color.setHex(0x337ab7);
			//document.getElementById("infos_capteur").innerHTML =  "coucou, tu as cliqué sur un capteur, les infos seront disponibles plus tard" ;
			var url = 'ajax/infosCapteur.php?id_capteur='+intersects[ 0 ].object.idC; // name == id dans la bdd
            $.post(url, function(data){
                $('#infos_capteur').html(data);
    		});
	          if (intersects[0].object.idC in selected) {
	            var sondecolor = selected[intersects[0].object.idC];
	            intersects[0].object.material = sondecolor[0];
	            delete selected[intersects[0].object.idC];
	            notifySondeDeleted(intersects[0].object.name, intersects[0].object.idC, intersects[0].object.idD);
	          } else {
	            selected[intersects[0].object.idC] = [intersects[0].object.material];
	            intersects[0].object.material = new THREE.MeshBasicMaterial({color:"#1D1D1D"});
	            notifySondeSelected(intersects[0].object.name, intersects[0].object.idC, intersects[0].object.idD);
	          }
		}
      }
      function notifySondeSelected(name, id, idD) {
        window.parent.postMessage("selected:" + name + "," + id + "," + idD, "*");
      }

      function notifySondeDeleted(name, id, idD) {
        window.parent.postMessage("deleted:" + name + "," + id + "," + idD, "*");
      }

      function animate() {
        requestAnimationFrame(animate);
        controls.update();
        render();
        update();
      }

      function render() {
        renderer.render(scene, camera);
      }

      function update() {
        raycaster.setFromCamera(mouse, camera);
        var intersects = raycaster.intersectObjects( objects );
        if (intersects.length > 0) {
          if (intersects[0].object.name != labelIntersected) {
            label.innerHTML = intersects[0].object.name;
            labelIntersected = intersects[0].object.name;
            label.style.visibility = "visible";
          }
        } else if (labelIntersected != "") {
          labelIntersected = "";
          label.style.visibility = "hidden";
        }
      }

      function reset() {
        controls.reset();
      }

      function zoomPlus(){
        controls.dollyIn(1.2);
      }

      function zoomMoins(){
        controls.dollyOut(1.2);
      }
		function getElementsByClass(searchClass,node,tag) {
			var classElements = new Array();
			if ( node == null )
				node = document;
			if ( tag == null )
				tag = '*';
			var els = node.getElementsByTagName(tag);
			var elsLen = els.length;
			var pattern = new RegExp("(^|\\s)"+searchClass+"(\\s|$)");
			for (i = 0, j = 0; i < elsLen; i++) {
				if ( pattern.test(els[i].className) ) {
					classElements[j] = els[i];
					j++;
				}
			}
			return classElements;
		}
        function actualiser(posX,posY,posZ){
          	if(scene.getObjectByName("corbeille_provisoire") == null){
	          //scene.remove(scene.getObjectByName("corbeille_provisoire"));
	          var loader = new THREE.OBJMTLLoader();
	          loader.load( 'ajax/objects/corbeille.obj', 'ajax/objects/corbeille.mtl', function ( object ) {
	            object.position.y = posX;
	            object.position.x = posY;
	            object.position.z = posZ;
	            object.name="corbeille_provisoire";
	            scene.add( object );
	          });
      		}
      		else{
      			scene.getObjectByName("corbeille_provisoire").position.x = posX;
      			scene.getObjectByName("corbeille_provisoire").position.y = posY;
      			scene.getObjectByName("corbeille_provisoire").position.z = posZ;
      		}
        }
        function actualiserCapteur(posX,posY,posZ){
          //scene.remove(scene.getObjectByName("capteur_provisoire"));
          if(scene.getObjectByName("capteur_provisoire") == null){
          	addCapteur(posX,posY,posZ,-1,-1,'capteur_provisoire');
          }else{
          	scene.getObjectByName("capteur_provisoire").position.x = posX;
          	scene.getObjectByName("capteur_provisoire").position.y = posY;
          	scene.getObjectByName("capteur_provisoire").position.z = posZ;
          }
        }
		</script>
		<div id="infos_capteur"> </div>	
</body>
</html>