<script>
	$(document).ready(function()
	{
		$(".draggable").draggable();
	});

	function popUpInfo(nomC, nomD, unite, temps)
	{
		alert(	"Le nom du capteur est : " + nomC + 
			"\nLe dispositif est : " + nomD + 
			"\nL'unité utilisée par le capteur : " + unite + 
			"\nLe temps d'actualisation est : " + temps +"s");
	}

	function deleteSensor(arduinoID, port)
	{
		var data = {idA: arduinoID , port: port};
		$.ajax(
		{
			url : "admin/deleteBranchement.php",
			type: "POST",
			data: data,
		});

		var formData = {device: $("#deviceName :selected").val()};
		$.ajax(
		{
			url : "admin/loadSensor.php",
			type: "POST",
			data : formData,
			success: function(data, status)
			{
				$("#resDevice").html(data);
			}
		});

		var dat = {arduino: $("#arduinoName :selected").val()};
		$.ajax(
		{
			url : "admin/loadArduino.php",
			type: "POST",
			data : dat,
			success: function(data, status)
			{
				$("#resArduino").html(data);
			}
		});

		$.ajax(
		{
			url : "admin/timeUpdate.php",
			type : "POST",
			success: function(data, status)
			{
				$("#resConnected").html(data);
			}
		});

	}
</script>

<?php
require_once "GestionBase.php";

// Affiche les capteur d'un dispositif
if( isset($_POST["device"]) )
{
	$id = getIdByName($_POST["device"]);
	foreach ($id as $key) 
	{
		$stock = $key;
	}
	$sensor = getSensorByDevice($stock);

	// Verification si le capteur est déja connecté
	$arrayIdSensor = array();

	foreach (getBranchement() as $k)
	{
		array_push($arrayIdSensor, $k["idC"]);
	}

	if ($sensor)
	{
		echo "<div id=res>";
		foreach ($sensor as $val) 
		{
			$nomCapteur = $val["nomC"];
			$idDispo = $val["idD"];
			foreach (getNameById($idDispo) as $key)
			{
				$nameDispo = $key;
			}
			$unite = $val["unite"];
			$nivProf = $val["nivProfond"];


			if ( !(in_array($val["idC"], $arrayIdSensor)) )
			{
				echo "<ul id='listcapteurs'>";
				if ( $val['typeC'] == "A")
				{
					echo "<div id='".$val['idC']."' class='capteur ui-widget-content draggable analogSensor'><p>".$val['nomC']."</p>";
				// bleu Analogique
					echo "<input type='button' id='".$val['idC']."' name='info' class='info' onclick='popUpInfo(\"".str_replace('"', '\"', $nomCapteur)."\",\"".str_replace('"', '\"', $nameDispo)."\",\"".str_replace('"', '\"', $unite)."\",\"".$nivProf."\");'>";
					echo "</div>";
				}
				else
				{
					echo "<div id='".$val['idC']."' class='capteur ui-widget-content draggable numericSensor'><p>".$val['nomC']."</p>";
				// Vert Numerique
					echo "<input type='button' id='".$val['idC']."' value='info' name='info' class='info ui-widget-content draggable numericSensor' onclick='popUpInfo(\"".str_replace('"', '\"', $nomCapteur)."\",\"".str_replace('"', '\"', $nameDispo)."\",\"".str_replace('"', '\"', $unite)."\",\"".$nivProf."\");'>";
					echo "</div>";
				}
				echo "</ul>";
			}
		}
		echo "</div>";
	}
	else
	{
		echo "<p> Aucun capteur </p>";
	}
}
?>
