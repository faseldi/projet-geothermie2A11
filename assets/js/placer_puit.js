  var mat = new THREE.MeshBasicMaterial({ color: "rgb(44, 62, 80)", wireframe:false });
  var Geo = new THREE.CylinderGeometry(10/2, 10/2, 55, 8, 1); 
  var Geo2 = new THREE.CylinderGeometry(10/2, 10/2, 805, 8, 1); 
  var Geo3 = new THREE.CylinderGeometry(10/2, 10/2, 214/2, 8, 1); 
  var Geo4 = new THREE.CylinderGeometry(10/2, 10/2, 24, 8, 1);
  var Geo5 = new THREE.CylinderGeometry(5,5,200,8,1);
  var Geo6 = new THREE.CylinderGeometry(5,5,200,8,1);
  var Geo7 = new THREE.CylinderGeometry(5,5,200,8,1);
  var p = new THREE.Mesh(Geo, mat);
  p.position.set(850/2, 380/2-XOFFSET, 550/2);
  p.name = "Puit Canadien";
  scene.add(p);

  var p1 = new THREE.Mesh(Geo2, mat);
  p1.position.set(50/2, 275/2-XOFFSET, 550/2);
  p1.rotation.z = 94 * (Math.PI / 180);
  p1.name = "Puit Canadien";
  scene.add(p1);

  var p2 = new THREE.Mesh(Geo3, mat);
  p2.position.set(-800/2, 305/2-XOFFSET, 550/2);
  p2.rotation.z = 28 * (Math.PI / 180);
  p2.name = "Puit Canadien";
  scene.add(p2);
  
  var p3 = new THREE.Mesh(Geo4, mat);
  p3.position.set(-850/2, 420/2-XOFFSET, 550/2);
  p3.name = "Puit Canadien";
  scene.add(p3);

  var dir = new THREE.Vector3( 1, 0, 0 );
  var origin = new THREE.Vector3( 0, 0, 0 );
  var length = 200;
  var hex = 0xff0000;
  var arrowX= new THREE.ArrowHelper( dir, origin, length, hex );
  arrowX.position.set(0, -XOFFSET, 3);
  scene.add(arrowX);
  
  var dir = new THREE.Vector3( 0, 0, 1 );
  var origin = new THREE.Vector3( 0, 0, 0 );
  var length = 200;
  var hex = 0x00ff00;
  var arrowZ= new THREE.ArrowHelper( dir, origin, length, hex );
  arrowZ.position.set(0, -XOFFSET, 3);
  scene.add(arrowZ);
  
  var dir = new THREE.Vector3( 0, 1, 0 );
  var origin = new THREE.Vector3( 0, 0, 0 );
  var length = 200;
  var hex = 0x000ff;
  var arrowY = new THREE.ArrowHelper( dir, origin, length, hex );
  arrowY.position.set(0, -XOFFSET, 0);
  scene.add(arrowY);