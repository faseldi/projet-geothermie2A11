<?php session_start(); ?>
<!DOCTYPE html>
<html lang="fr">
<head>

  <?php
    $title = "Administration";
    include('includes/layout/head.php');
    include('admin/GestionBase.php');
  ?>

  <!-- POUR LA SELECTION A VOIR SI IL FAUT GARDER -->
  <link rel="stylesheet" href="assets/vendor/jquery/jquery-ui.css">
  <!-- <link rel="stylesheet" href="assets/vendor/jquery/jquery.ui.theme.css"> -->
  <script type="text/javascript" src="assets/vendor/jquery/jquery-ui.js"></script>
  <!-- !! -->

  <script type="text/javascript" src="assets/vendor/tabs/js/easyResponsiveTabs.js"></script>
  <link rel="stylesheet" href="assets/vendor/tabs/css/easy-responsive-tabs.css">

  <style>
    .ui-selecting { background: #FECA40; }
    .ui-selected { background: #F39814; color: white; }
  </style>

  <script type="text/javascript">
    $(document).ready(function () { 
      $(function() {
        $( "#selectable" ).selectable();
        $( "#selectable2" ).selectable();
        $("#selectable3").selectable();
      });

    $('#supprCapteur').click(function () {
      $('#selectable .ui-widget-content.ui-selected').each(function(index) {
        var tmp = $(this).attr('data-userid');
        $.post('admin/supprCapteur.php', { id : tmp })
          .done(function (data) { location.reload(); })
          .fail(function (data) {});
      });
    });

    $('#supprDispositif').click(function () {
      $('#selectable2 .ui-widget-content.ui-selected').each(function(index) {
        var tmp = $(this).attr('data-userid');
        var all = 'non';
        if (document.getElementById('tout').checked) {
		  all = 'oui';
	    }
        $.post('admin/supprDispositif.php', { id : tmp, tout : all })
          .done(function (data) { location.reload(); })
          .fail(function (data) {});
      });
    });

    $('#supprArduino').click(function () {
      $('#selectable3 .ui-widget-content.ui-selected').each(function(index) {
        var tmp = $(this).attr('data-userid');
        $.post('admin/supprArduino.php', { id : tmp })
          .done(function (data) { location.reload(); })
          .fail(function (data) {});
      });
    });

    <?php
      $res = infoDispositif();
      while($data = $res->fetch(PDO::FETCH_ASSOC)) {
        echo "$('#sondeCorbeille').append('<option value=" . $data['idD'] . ">" . $data['nomD'] . "');\n";
      }

    ?>
    });
  </script>
</head>

<body>
  <?php include('includes/layout/header.php'); ?>

  <?php include("admin/pass.php"); ?> <!-- Verifie le mot de passe de l'admin -->

  <div class="container">
  <div class="wrapper">
  <h2>Administration</h2>
    <?php
    if($_SESSION['pass'] == false) {
    ?>

    <div class="login-box four columns offset-by-four">
      <p>Veuillez entrer le mot de passe !</p>
      <form style="text-align: center" action="#" method="post">
        <input type="password" name="pass" class="u-full-width" value=""/>   
        <input type="submit" value="Valider"/>
      </form>
    </div>

    <?php } else { ?>

    <div id="parentTab">
      <ul class="resp-tabs-list hor_1">
        <li onmousedown="changerVue(this.id)" id="gC">Capteurs</li>
        <li onmousedown="changerVue(this.id)" id="gD">Dispositifs</li>
        <li>Arduino</li>
        <li>Branchements Arduino</li>
        <li>Export Données</li>
      </ul>
      <script>
        function changerVue(id){
          if(id == "gC"){
            if($("#iframe2").attr('src') != 'vue3D.php'){
              $("#iframe2").attr('src', 'vue3D.php');
            }
          }else{
            if($("#iframe").attr('src') != 'vue3D.php'){
              $("#iframe").attr('src', 'vue3D.php');
            }
          }
        }
      </script>
      <div class="resp-tabs-container hor_1">
        <div>
          <br>
          <form action="admin/ajouterCapteur.php" method="post">
            <h6>Ajouter un capteur</h6>
            <div id="new">
              <p> ID, vide si nouveau : </p>
              <input type="number" value="" name="idDuCapteur" onclick="nouveau()" title="cliquez pour vider" readonly>
            </div>
            <style>
            #new{
              display: inline-flex;
            }
            #new p{
              padding-right: 20px;
            }
            </style>
            <script type="text/javascript">
              function nouveau(){
                document.getElementsByName('idDuCapteur')[0].value = "";
              }
            </script>
            <div class="row">
              <div class="six columns">
                <label for="nomCap">Nom du capteur : </label>
                <input type="text" required="required" class="u-full-width" name="nomCap" id="nomCap">
              </div>
              <div class="six columns">
                <label for="relier">Dispositif parent : </label>
                <select name="relier" required="required" class="u-full-width" id="sondeCorbeille"></select>
              </div>
            </div>
            <div class="row">
			  <div class="six columns">
				<label for="type">Type de capteur :</label>
                <select name="type" required="required" class="u-full-width" id="typesCap">
                  <option value="A">Analogique</option>
                  <option value="N">Numerique</option>
                </select>
			  </div>
			  <div class="six columns">
			    <label for="unite">Unite du capteur :</label>
			    <select name="unite" required="required" class="u-full-width" id="typesUni">
                  <option value="tempe">Temperature °C</option>
                  <option value="press">Pression</option>
                  <option value="debit">Débit</option>
                </select>
			  </div>
            </div>
            <div class="row">
              <div class="four columns">
                <label for="posx">axe X :</label>
                <input type="number" required="required" class="u-full-width" name="positionCapteurX" oninput="actualiserCapteur()" id="posCx" value="0">
              </div>
              <div class="four columns">
                <label for="posz">axe Y :</label>
                <input type="number" required="required" class="u-full-width" name="positionCapteurY" oninput="actualiserCapteur()" id="posCz" value="0">
              </div>
              <div class="four columns">
                <label for="posy">axe Z :</label>
                <input type="number" required="required" class="u-full-width" name="positionCapteurZ" oninput="actualiserCapteur()" id="posCy" value="0">
              </div>
              <input type="button" value="Ajouter" class="button-primary u-pull-right" onclick="ajouterCapteur()" />
            </div>
          </form>
          <div id="label"></div>
          <iframe id="iframe2" class="u-full-width" src="vue3D.php"></iframe>
          <script>
          function ajouterCapteur(){
            var posX = document.getElementsByName('positionCapteurX')[0].value;
            var posY = document.getElementsByName('positionCapteurY')[0].value;
            var posZ = document.getElementsByName('positionCapteurZ')[0].value;
            var nom = document.getElementsByName('nomCap')[0].value;
            var id = document.getElementsByName('idDuCapteur')[0].value;
            var typeU = document.getElementById('typesUni').value;
            var typeCap = document.getElementById('typesCap').value;
            var idD = document.getElementById('sondeCorbeille').value;
            var url = 'ajax/modifications.php?posX='+posX+'&posY='+posY+'&posZ='+posZ+'&nom='+nom+'&typeU='+typeU+'&typeCap='+typeCap+'&idD='+idD;
            if(id == ''){
              $.ajax({
                url: url,
                success: function(data){
                console.log('ajout success');
                }
              });
              return;
            }
            console.log(posX+" "+posY+" "+posZ+" "+nom+" "+id);
            var url = 'ajax/modifications.php?posX='+posX+'&posY='+posY+'&posZ='+posZ+'&nom='+nom+'&id='+id+'&cap=temp';
            $.ajax({
             url: url,
             success: function(data){
              console.log('modification success');
             }
           });
            var posX = document.getElementsByName('positionCapteurX')[0].value = '';
            var posY = document.getElementsByName('positionCapteurY')[0].value = '';
            var posZ = document.getElementsByName('positionCapteurZ')[0].value = '';
            var nom = document.getElementsByName('nomCap')[0].value = '';
            var id = document.getElementsByName('idDuCapteur')[0].value = '';
          }
          function actualiserCapteur(){
            var posX = document.getElementsByName('positionCapteurX')[0].value;
            var posY = document.getElementsByName('positionCapteurY')[0].value;
            var posZ = document.getElementsByName('positionCapteurZ')[0].value;
            window.frames[0].frameElement.contentWindow.actualiserCapteur(posX,posY,posZ);
          }
          function cliqueCapteur(id){
              chargerCapteur(id);
          }
          function cliqueCorbeille(id){
            if(document.getElementById('gD').classList.contains('resp-tab-active')){
              chargerCorbeille(id);
            }
          }
          $('#iframe').height($('#iframe').width() / ($(window).width() / $(window).height()) +($(window).height())/5);
          $('#iframe2').height($('#iframe2').width() / ($(window).width() / $(window).height()) +($(window).height())/5);
          </script>
          <hr>
          <h6>Supprimer ou modifier des capteurs</h6>
          <div class="tabs-listeSondes">
            <ul id="selectable">
            <?php
              $res = getCapteursPuits();
              while($data = $res->fetch(PDO::FETCH_ASSOC)) {
                echo '<li onmousedown="chargerCapteur(this.id);" id="'.$data['idC'].'" class="ui-widget-content" data-userid="' . $data['idC'] . '">' . $data['nomC'] . '</li>';
              }
            ?>
            </ul>
          </div>
          <div id="capteurs"></div>
          <script type="text/javascript" src"functions_ajax.js"></script>
          <script type="text/javascript">
            Element.prototype.remove = function() {
              this.parentElement.removeChild(this);
            }
            NodeList.prototype.remove = HTMLCollection.prototype.remove = function() {
              for(var i = this.length - 1; i >= 0; i--) {
                if(this[i] && this[i].parentElement) {
                  this[i].parentElement.removeChild(this[i]);
                }
              }
            }
            function getElementsByClass(searchClass,node,tag) {
              var classElements = new Array();
              if ( node == null )
                node = document;
              if ( tag == null )
                tag = '*';
              var els = node.getElementsByTagName(tag);
              var elsLen = els.length;
              var pattern = new RegExp("(^|\\s)"+searchClass+"(\\s|$)");
              for (i = 0, j = 0; i < elsLen; i++) {
                if ( pattern.test(els[i].className) ) {
                  classElements[j] = els[i];
                  j++;
                }
              }
              return classElements;
            }
            function chargerCapteur(id){
              var url = 'ajax/getCapteursAjax.php';
              var x,y,z;
              $.post(url, function(data){
                  $('#capteurs').html(data);
                  var posX = getElementsByClass("posX");
                  var posY = getElementsByClass("posY");
                  var posZ = getElementsByClass("posZ");
                  var noms = getElementsByClass("noms");
                  var ids = getElementsByClass("ids");
                  var types = getElementsByClass("types");  
                  var unites = getElementsByClass("unites"); 
                  for(var i = 0; i < posX.length; i++){           
                    if(ids[i].id == id){
                      document.getElementsByName('positionCapteurX')[0].value = document.getElementById(posX[i].id).textContent;
                      document.getElementsByName('positionCapteurY')[0].value = document.getElementById(posY[i].id).textContent;
                      document.getElementsByName('positionCapteurZ')[0].value = document.getElementById(posZ[i].id).textContent;
                      document.getElementsByName('nomCap')[0].value = document.getElementById('noms'+id).textContent;
                      document.getElementsByName('idDuCapteur')[0].value = ids[i].id;
                      var type = document.getElementById(types[i].id).textContent;
                      var unite = unescape(decodeURIComponent(document.getElementById(unites[i].id).textContent));
                      if(unite == "Pa"){
                         document.getElementById('typesUni').value = "press";
                      }
                      if(type != 'A'){
                        document.getElementById('typesCap').value = "N";
                      }
                      break;
                    }
                  }
                  document.getElementById("capteurs").innerHTML ="";
              });
            }
            function chargerCorbeille(id){
              var url = 'ajax/getDevicesAjax.php';
              var x,y,z;
              $.post(url, function(data){
                  $('#capteurs').html(data);
                  var posX = getElementsByClass("posX");
                  var posY = getElementsByClass("posY");
                  var posZ = getElementsByClass("posZ");
                  var noms = getElementsByClass("noms");
                  var ids = getElementsByClass("idCorb");
                  //var types = getElementsByClass("types"); 
                  var unites = getElementsByClass("unites");  
                  for(var i = 0; i < posX.length; i++){      
                    if(ids[i].textContent == id){
                      document.getElementsByName('positionCorbeilleX')[0].value = document.getElementById(posX[i].id).textContent;
                      document.getElementsByName('positionCorbeilleY')[0].value = document.getElementById(posY[i].id).textContent;
                      document.getElementsByName('positionCorbeilleZ')[0].value = document.getElementById(posZ[i].id).textContent;
                      document.getElementsByName('nom')[0].value = document.getElementById('noms'+id).textContent;
                      document.getElementsByName('idCorbeille')[0].value = ids[i].textContent;
                      //var type = document.getElementById(types[i].id).textContent;
                      break;
                    }
                  }
                  document.getElementById("capteurs").innerHTML ="";
              });
            }
          </script>
          <button type="button" class="button-primary u-pull-right" id="supprCapteur">Supprimer</button>
          <div class="u-cf"></div>
        </div>
        <div>
          <br>
          <form action="admin/ajouterDispositif.php" method="post">
            <h6>Ajouter un dispositif</h6>
            <div id="newC">
              <p> ID, vide si nouveau : </p>
              <input type="number" value="" name="idCorbeille" onclick="nouvelle()" title="cliquez pour vider" readonly>
            </div>
            <style>
            #newC{
              display: inline-flex;
            }
            #newC p{
              padding-right: 20px;
            }
            </style>
            <script type="text/javascript">
              function nouvelle(){
                document.getElementsByName('idCorbeille')[0].value = "";
              }
            </script>
            <div class="row">
              <div class="six columns">
                <label for="nom">Nom du dispositif :</label>
                <input type="text" class="u-full-width" name="nom" id="nom">
              </div>
              <div class="six columns">
                <label for="type">Type du dispositif :</label>
                <select name="type" class="u-full-width">
                  <option value="corbeille">Corbeille</option>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="twelve columns">
                <label for="lieu">Lieu :</label>
                <select name="lieu" class="u-full-width">
                  <option value="puit">À côté de la cafétéria</option>
                  <option value="devantGTE">Devant le batiment GTE</option>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="four columns">
                <label for="posx">axe X :</label>
                <input type="number" required="required" class="u-full-width" name="positionCorbeilleX" id="posx" oninput="actualiser()" value="0">
              </div>
              <div class="four columns">
                <label for="posz">axe Y :</label>
                <input type="number" required="required" class="u-full-width" name="positionCorbeilleY" id="posz" oninput="actualiser()" value="0">
              </div>
              <div class="four columns">
                <label for="posy">axe Z :</label>
                <input type="number" required="required" class="u-full-width" name="positionCorbeilleZ" id="posy" oninput="actualiser()" value="0">
              </div>
              <input type="button" onclick="modifierCorbeille()" value="Ajouter" class="button-primary u-pull-right" />
            </div>
          </form>
          <iframe id="iframe" class="u-full-width"  ></iframe>
          <script>
            function modifierCorbeille(){
              var posX = document.getElementsByName('positionCorbeilleX')[0].value;
              var posY = document.getElementsByName('positionCorbeilleY')[0].value;
              var posZ = document.getElementsByName('positionCorbeilleZ')[0].value;
              var lieu = document.getElementsByName('lieu')[0].value;
              var nom = document.getElementById('nom').value;
              var id = document.getElementsByName('idCorbeille')[0].value;
              var url = 'ajax/modifications.php?posX='+posX+'&posY='+posY+'&posZ='+posZ+'&nom='+nom+'&disp=corb'+'&lieu='+lieu;
              if(id == ''){
                $.ajax({
                  url: url,
                  success: function(data){
                  console.log('ajout success');
                  }
                });
                return;
              }
              var url = 'ajax/modifications.php?posX='+posX+'&posY='+posY+'&posZ='+posZ+'&nom='+nom+'&disp=corb'+'&lieu='+lieu+'&id='+id;
              $.ajax({
               url: url,
               success: function(data){
                console.log('modification success'+data);
               }
             });
              var posX = document.getElementsByName('positionCorbeilleX')[0].value = '';
              var posY = document.getElementsByName('positionCorbeilleY')[0].value = '';
              var posZ = document.getElementsByName('positionCorbeilleZ')[0].value = '';
              var nom = document.getElementById('nom').value = '';
              var id = document.getElementsByName('idCorbeille')[0].value = '';
            }
            function actualiser(){
              var posX = document.getElementsByName('positionCorbeilleX')[0].value;
              var posY = document.getElementsByName('positionCorbeilleY')[0].value;
              var posZ = document.getElementsByName('positionCorbeilleZ')[0].value;
              window.frames[1].frameElement.contentWindow.actualiser(posX,posY-30,posZ); // 1 car il y en a deux sur la mm page, -15 pour la motié d'une corbeille
            }
            $('#iframe').height($('#iframe').width() / ($(window).width() / $(window).height()));
          </script>
          <hr>
          <h6>Supprimer ou modifier des dispositifs</h6>
          <div class="tabs-listeSondes">
            <ul id="selectable2">
              <?php
                $res = infoDispositif();
                while($data = $res->fetch(PDO::FETCH_ASSOC)){
                  if($data['typeD'] == "corbeille" && $data['lieu'] == "puit"){
                    echo '<li onmousedown="chargerCorbeille(this.id);" id="'.$data['idD'].'" class="ui-widget-content" data-userid="' . $data['idD'] . '">' . $data['typeD'] . ' -- ' . $data['nomD'] . '</li>';
                  }
                }
              ?>
            </ul>
          </div>
          <input type="checkbox" name="tout" id="tout" value="true" /> Supprimer les capteurs liés aux dispositifs.
          <button type="button" class="button-primary u-pull-right" id="supprDispositif">Supprimer</button>
          <div class="u-cf"></div>
        </div>
        <div>
          <h6>Ajouter un arduino</h6>
          <form name="chgFreq" action="admin/ajouterArduino.php" method="post">
            <div class="row">
              <div class="six columns">
                <label for="nom">Nom de l'arduino :</label>
                <input type="text" class="u-full-width" name="nom" id="nom">
              </div>
              <div class="six columns">
                <label for="address">Adresse Mac :</label>
                <input type="text" class="u-full-width" name="address" id="address">
              </div>
            </div>
            <div class="row">
              <div class="six columns">
                <label for="nbAnalogique">Nombre de ports analogiques :</label>
                <input type="text" class="u-full-width" name="nbAnalogique" id="nbAnalogique">
              </div>
              <div class="six columns">
                <label for="nbNumerique">Nombre de ports numériques :</label>
                <input type="text" class="u-full-width" name="nbNumerique" id="nbNumerique">
              </div>
            </div>
            <input type="submit" class="button-primary u-pull-right" value="Ajouter">
          </form>
          <div class="u-cf"></div>
          <h6>Supprimer des arduino</h6>
          <div class="tabs-listeSondes">
            <ul id="selectable3">
            <?php
              $res = infoArduino();
              while($data = $res->fetch(PDO::FETCH_ASSOC)) {
                echo '<li class="ui-widget-content" data-userid="' . $data['idA'] . '">' . $data['nom'] . '</li>';
              }
            ?>
            </ul>
          </div>
          <button type="button" class="button-primary u-pull-right" id="supprArduino">Supprimer</button>
          <div class="u-cf"></div>
        </div>
        <div>
          <div id="branchements">
            <!--<h6>Création des branchements</h6>
            <a href="arduino.php">Accèder à l'interface de modification des branchements</a>-->
            <!--<script>$("#branchements").load('arduino.php');</script>-->
            <iframe src="arduino.php" width="100%" height="800px"></iframe>
            </iframe>
          </div>
        </div>
        <div>
          <h6>Export de la base :</h6>
          <form name="export" action="admin/backupBase.php" method="post">
            <div class="row">
              <div class="twelve columns">
                <label for="filename">Nom du fichier :</label>
                <input type="text" name="filename" class="u-full-width">
              </div>
            </div>
            <input type="hidden" name="action" value="export">
            <input type="submit" class="button-primary u-pull-right" value="Valider">
          </form>
          <div class="u-cf"></div>
        </div>
      </div>
    </div>
    <?php } ?>
    <div class="push"></div>
    <div class="push"></div>
  </div> <!-- wrapper -->
  </div> <!-- container -->

<?php include('includes/layout/footer.php');  ?>

  <script type="text/javascript">
    $(document).ready(function() {
        //Horizontal Tab
        $('#parentTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            tabidentify: 'hor_1', // The tab groups identifier
            active_border_color: '#e1e1e1', // border color for active tabs heads in this group
            active_content_border_color: '#E94F51',
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#nested-tabInfo');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });
    });
  </script>

</body>
</html>
